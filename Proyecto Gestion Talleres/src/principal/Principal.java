package principal;

import MVC.*;

public class Principal {
    public static void main(String[] args) {
        VistaAcceso vistaAcceso = new VistaAcceso();
        Vista vista = new Vista();
        VistaGestionUsuarios vistaGestionUsuarios = new VistaGestionUsuarios();
        Modelo unModelo = new Modelo();
        Controlador controlador = new Controlador(vistaAcceso,vista,vistaGestionUsuarios,unModelo);
    }
}
