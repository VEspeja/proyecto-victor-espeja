package base;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class Usuario {
    private int id;
    private String nameUser;
    private String pass;
    private int rol;

    public Usuario() {
    }
    public Usuario(String nameUser, String pass, int rol) {
        this.nameUser = nameUser;
        this.pass = pass;
        this.rol = rol;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name_user",unique = true)
    public String getNameUser() {
        return nameUser;
    }

    public void setNameUser(String nameUser) {
        this.nameUser = nameUser;
    }

    @Basic
    @Column(name = "pass")
    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    @Basic
    @Column(name = "rol")
    public int getRol() {
        return rol;
    }

    public void setRol(int rol) {
        this.rol = rol;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Usuario usuario = (Usuario) o;
        return id == usuario.id &&
                rol == usuario.rol &&
                Objects.equals(nameUser, usuario.nameUser) &&
                Objects.equals(pass, usuario.pass);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nameUser, pass, rol);
    }

    @Override
    public String toString() {
        return "User= " + nameUser + " - " + rol;
    }
}
