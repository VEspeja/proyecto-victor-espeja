package base;

import javax.persistence.*;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
public class Vehiculo {
    private int id;
    private String modelo;
    private String marca;
    private String matricula;
    private String combustible;
    private Date fechaMatriculacion;
    private String numeroChasis;
    private String imagen;
    private Cliente cliente;
    private List<Reparacion> listaReparaciones;

    public Vehiculo(int id, String modelo, String marca, String matricula, String combustible, LocalDate fechaMatriculacion, String numeroChasis, String imagen, Cliente cliente) {
        this.id = id;
        this.modelo = modelo;
        this.marca = marca;
        this.matricula = matricula;
        this.combustible = combustible;
        this.fechaMatriculacion = Date.valueOf(fechaMatriculacion);
        this.numeroChasis = numeroChasis;
        this.imagen = imagen;
        this.cliente = cliente;
        this.listaReparaciones = new ArrayList<>();
    }

    public Vehiculo() {
        this.listaReparaciones = new ArrayList<>();
    }


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "modelo")
    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    @Basic
    @Column(name = "marca")
    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    @Basic
    @Column(name = "matricula",unique = true)
    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    @Basic
    @Column(name = "combustible")
    public String getCombustible() {
        return combustible;
    }

    public void setCombustible(String combustible) {
        this.combustible = combustible;
    }

    @Basic
    @Column(name = "fecha_matriculacion")
    public Date getFechaMatriculacion() {
        return fechaMatriculacion;
    }

    public void setFechaMatriculacion(Date fechaMatriculacion) {
        this.fechaMatriculacion = fechaMatriculacion;
    }

    @Basic
    @Column(name = "numero_chasis", unique = true)
    public String getNumeroChasis() {
        return numeroChasis;
    }

    public void setNumeroChasis(String numeroChasis) {
        this.numeroChasis = numeroChasis;
    }

    @Basic
    @Column(name = "imagen")
    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vehiculo vehiculo = (Vehiculo) o;
        return id == vehiculo.id &&
                Objects.equals(modelo, vehiculo.modelo) &&
                Objects.equals(marca, vehiculo.marca) &&
                Objects.equals(matricula, vehiculo.matricula) &&
                Objects.equals(combustible, vehiculo.combustible) &&
                Objects.equals(fechaMatriculacion, vehiculo.fechaMatriculacion) &&
                Objects.equals(numeroChasis, vehiculo.numeroChasis) &&
                Objects.equals(imagen, vehiculo.imagen);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, modelo, marca, matricula, combustible, fechaMatriculacion, numeroChasis, imagen);
    }

    @ManyToOne
    @JoinColumn(name = "id_propietario", referencedColumnName = "id")
    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    @OneToMany(mappedBy = "vehiculo")
    public List<Reparacion> getListaReparaciones() {
        return listaReparaciones;
    }

    public void setListaReparaciones(List<Reparacion> listaReparaciones) {
        this.listaReparaciones = listaReparaciones;
    }

    @Override
    public String toString() {
        return "Id=" + id +
                " - " + modelo +
                " - " + marca +
                " - " + matricula;
    }

    public String obtenerDatos() {
        return "Id=" + id + " - " + matricula;
    }
}
