package base;

import javax.persistence.*;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
public class Taller {
    private int id;
    private String nombre;
    private String ubicacion;
    private int telefono;
    private Date fechaApertura;
    private String horarioApertura;
    private String horarioCierre;
    private boolean tiendaOnline;
    private List<Mecanico> listaMecanicos;


    public Taller(String nombre, String ubicacion, int telefono, LocalDate fechaApertura, String horarioApertura, String horarioCierre, boolean tiendaOnline) {
        this.nombre = nombre;
        this.ubicacion = ubicacion;
        this.telefono = telefono;
        this.fechaApertura = Date.valueOf(fechaApertura);
        this.horarioApertura = horarioApertura;
        this.horarioCierre = horarioCierre;
        this.tiendaOnline = tiendaOnline;
        this.listaMecanicos = new ArrayList<>();
    }

    public Taller() {
        this.listaMecanicos = new ArrayList<>();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "ubicacion")
    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    @Basic
    @Column(name = "telefono")
    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    @Basic
    @Column(name = "fecha_apertura")
    public Date getFechaApertura() {
        return fechaApertura;
    }

    public void setFechaApertura(Date fechaApertura) {
        this.fechaApertura = fechaApertura;
    }

    @Basic
    @Column(name = "horario_apertura")
    public String getHorarioApertura() {
        return horarioApertura;
    }

    public void setHorarioApertura(String horarioApertura) {
        this.horarioApertura = horarioApertura;
    }

    @Basic
    @Column(name = "horario_cierre")
    public String getHorarioCierre() {
        return horarioCierre;
    }

    public void setHorarioCierre(String horarioCierre) {
        this.horarioCierre = horarioCierre;
    }

    @Basic
    @Column(name = "tienda_online")
    public boolean getTiendaOnline() {
        return tiendaOnline;
    }

    public void setTiendaOnline(boolean tiendaOnline) {
        this.tiendaOnline = tiendaOnline;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Taller taller = (Taller) o;
        return id == taller.id &&
                telefono == taller.telefono &&
                tiendaOnline == taller.tiendaOnline &&
                Objects.equals(nombre, taller.nombre) &&
                Objects.equals(ubicacion, taller.ubicacion) &&
                Objects.equals(fechaApertura, taller.fechaApertura) &&
                Objects.equals(horarioApertura, taller.horarioApertura) &&
                Objects.equals(horarioCierre, taller.horarioCierre);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nombre, ubicacion, telefono, fechaApertura, horarioApertura, horarioCierre, tiendaOnline);
    }

    @OneToMany(mappedBy = "taller")
    public List<Mecanico> getListaMecanicos() {
        return listaMecanicos;
    }

    public void setListaMecanicos(List<Mecanico> listaMecanicos) {
        this.listaMecanicos = listaMecanicos;
    }

    @Override
    public String toString() {
        return "Id= " + id +
                " - " + nombre +
                " - " + telefono;
    }
}
