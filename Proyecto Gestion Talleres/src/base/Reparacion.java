package base;

import javax.persistence.*;
import java.sql.Date;
import java.time.LocalDate;
import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
public class Reparacion {
    private int id;
    private Date fechaEmision;
    private double coste;
    private Date fechaFinalizacion;
    private String diagnostico;
    private Set<Mecanico> listaMecanicos;
    private Vehiculo vehiculo;

    public Reparacion() {
        this.listaMecanicos = new HashSet<>();
    }

    public Reparacion(LocalDate fechaEmision, double coste, LocalDate fechaFinalizacion, String diagnostico, Vehiculo vehiculo) {
        this.fechaEmision = Date.valueOf(fechaEmision);
        this.coste = coste;
        this.fechaFinalizacion = Date.valueOf(fechaFinalizacion);
        this.diagnostico = diagnostico;
        this.vehiculo = vehiculo;
        this.listaMecanicos = new HashSet<>();
    }


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "fecha_emision")
    public Date getFechaEmision() {
        return fechaEmision;
    }

    public void setFechaEmision(Date fechaEmision) {
        this.fechaEmision = fechaEmision;
    }

    @Basic
    @Column(name = "coste")
    public double getCoste() {
        return coste;
    }

    public void setCoste(double coste) {
        this.coste = coste;
    }

    @Basic
    @Column(name = "fecha_finalizacion")
    public Date getFechaFinalizacion() {
        return fechaFinalizacion;
    }

    public void setFechaFinalizacion(Date fechaFinalizacion) {
        this.fechaFinalizacion = fechaFinalizacion;
    }

    @Basic
    @Column(name = "diagnostico")
    public String getDiagnostico() {
        return diagnostico;
    }

    public void setDiagnostico(String diagnostico) {
        this.diagnostico = diagnostico;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Reparacion that = (Reparacion) o;
        return id == that.id &&
                Double.compare(that.coste, coste) == 0 &&
                Objects.equals(fechaEmision, that.fechaEmision) &&
                Objects.equals(fechaFinalizacion, that.fechaFinalizacion) &&
                Objects.equals(diagnostico, that.diagnostico);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, fechaEmision, coste, fechaFinalizacion, diagnostico);
    }

    @ManyToMany
    @JoinTable(name = "reparacion_mecanico", catalog = "", schema = "gestion_talleres", joinColumns = @JoinColumn(name = "id_reparacion", referencedColumnName = "id", nullable = false), inverseJoinColumns = @JoinColumn(name = "id_mecanico", referencedColumnName = "id", nullable = false))
    public Set<Mecanico> getListaMecanicos() {
        return listaMecanicos;
    }

    public void setListaMecanicos(Set<Mecanico> listaMecanicos) {
        this.listaMecanicos = listaMecanicos;
    }

    @ManyToOne
    @JoinColumn(name = "id_vehiculo", referencedColumnName = "id")
    public Vehiculo getVehiculo() {
        return vehiculo;
    }

    public void setVehiculo(Vehiculo vehiculo) {
        this.vehiculo = vehiculo;
    }

    @Override
    public String toString() {
        return "Id=" + id +
                " - " + coste +
                "€ - " + vehiculo.obtenerDatos();
    }


    public void asignarMecanicos(Collection<Mecanico> mecanicos){
        for (Mecanico mecanico : mecanicos){
            mecanico.getListaReparaciones().add(this);
        }
        this.listaMecanicos.addAll(mecanicos);
    }

    public void desvincularEntrenadores(Collection<Mecanico> mecanicos){
        for (Mecanico mecanico : mecanicos){
            mecanico.getListaReparaciones().remove(this);
        }
        this.listaMecanicos.removeAll(mecanicos);
    }

    public void desvincularTodos(){
        for(Mecanico mecanico : listaMecanicos){
            mecanico.getListaReparaciones().remove(this);
        }
        listaMecanicos.clear();
    }
}
