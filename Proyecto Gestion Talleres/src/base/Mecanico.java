package base;

import javax.persistence.*;
import java.sql.Date;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
public class Mecanico {
    private int id;
    private String nombre;
    private String apellidos;
    private String dni;
    private String numeroEmpleado;
    private Date fechaIncorporacion;
    private int anyosExperiencia;
    private String especialidad;
    private Taller taller;
    private Set<Reparacion> listaReparaciones;

    public Mecanico() {
        this.listaReparaciones = new HashSet<>();
    }

    public Mecanico(String nombre, String apellidos, String dni, String numeroEmpleado, LocalDate fechaIncorporacion, int anyosExperiencia, String especialidad, Taller taller) {
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.dni = dni;
        this.numeroEmpleado = numeroEmpleado;
        this.fechaIncorporacion = Date.valueOf(fechaIncorporacion);
        this.anyosExperiencia = anyosExperiencia;
        this.especialidad = especialidad;
        this.taller = taller;
        this.listaReparaciones = new HashSet<>();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "apellidos")
    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    @Basic
    @Column(name = "dni",unique = true)
    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    @Basic
    @Column(name = "numero_empleado")
    public String getNumeroEmpleado() {
        return numeroEmpleado;
    }

    public void setNumeroEmpleado(String numeroEmpleado) {
        this.numeroEmpleado = numeroEmpleado;
    }

    @Basic
    @Column(name = "fecha_incorporacion")
    public Date getFechaIncorporacion() {
        return fechaIncorporacion;
    }

    public void setFechaIncorporacion(Date fechaIncorporacion) {
        this.fechaIncorporacion = fechaIncorporacion;
    }

    @Basic
    @Column(name = "anyos_experiencia")
    public int getAnyosExperiencia() {
        return anyosExperiencia;
    }

    public void setAnyosExperiencia(int anyosExperiencia) {
        this.anyosExperiencia = anyosExperiencia;
    }

    @Basic
    @Column(name = "especialidad")
    public String getEspecialidad() {
        return especialidad;
    }

    public void setEspecialidad(String especialidad) {
        this.especialidad = especialidad;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Mecanico mecanico = (Mecanico) o;
        return id == mecanico.id &&
                anyosExperiencia == mecanico.anyosExperiencia &&
                Objects.equals(nombre, mecanico.nombre) &&
                Objects.equals(apellidos, mecanico.apellidos) &&
                Objects.equals(dni, mecanico.dni) &&
                Objects.equals(numeroEmpleado, mecanico.numeroEmpleado) &&
                Objects.equals(fechaIncorporacion, mecanico.fechaIncorporacion) &&
                Objects.equals(especialidad, mecanico.especialidad);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nombre, apellidos, dni, numeroEmpleado, fechaIncorporacion, anyosExperiencia, especialidad);
    }

    @ManyToOne
    @JoinColumn(name = "id_taller", referencedColumnName = "id")
    public Taller getTaller() {
        return taller;
    }

    public void setTaller(Taller taller) {
        this.taller = taller;
    }

    @ManyToMany(mappedBy = "listaMecanicos")
    public Set<Reparacion> getListaReparaciones() {
        return listaReparaciones;
    }

    public void setListaReparaciones(Set<Reparacion> listaReparaciones) {
        this.listaReparaciones = listaReparaciones;
    }

    @Override
    public String toString() {
        return "Mecanico = " + dni + " - Nombre: " + nombre
                + " - Taller: "+taller.getNombre();

    }

    public void desvincularTodos(){
        for(Reparacion reparacion : listaReparaciones){
            reparacion.getListaMecanicos().remove(this);
        }
        listaReparaciones.clear();
    }
}
