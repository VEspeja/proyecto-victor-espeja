package MVC;

import base.Usuario;

import javax.swing.*;
import java.awt.event.*;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class VistaCambioPassword extends JDialog {
    private JPanel contentPane;
    public JButton btnCancelar;
    private Modelo modelo;
    private Usuario usuario;
    private JPasswordField txtActualPass;
    private JPasswordField txtNewPass;
    private JPasswordField txtPassConfirm;
    public JLabel etPassActual;
    public JLabel etPassNueva;
    public JLabel etPassConfirmacion;
    public JButton btnConfirmar;
    private Properties selectorIdioma;

    public VistaCambioPassword(Modelo modelo, Usuario usuario, Properties selectorIdioma) {
        System.out.println("he llegadooo");
        System.out.println(usuario.getNameUser());
        this.modelo = modelo;
        this.usuario = usuario;
        this.selectorIdioma = selectorIdioma;
        init();
    }

    private void init() {
        setContentPane(contentPane);
        pack();
        setModal(true);
        getRootPane().setDefaultButton(btnConfirmar);
        this.setLocationRelativeTo(null);
        ImageIcon logo = new ImageIcon("imagenes/iconos/logo_aplicacion.png");
        setIconImage(logo.getImage());

        etPassActual.setText(selectorIdioma.getProperty("etPassActual"));
        etPassNueva.setText(selectorIdioma.getProperty("etPassNueva"));
        etPassConfirmacion.setText(selectorIdioma.getProperty("etPassConfirmacion"));
        btnConfirmar.setText(selectorIdioma.getProperty("btnConfirmar"));
        btnCancelar.setText(selectorIdioma.getProperty("btnCancelar"));
        setTitle(selectorIdioma.getProperty("ventanaCambioPass"));

        btnConfirmar.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                System.out.println("llego");
                onOK();
            }
        });

        btnCancelar.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });
        setVisible(true);




    }

    private void onOK() {
        if(this.usuario.getPass().equals(new String(this.txtActualPass.getPassword()))){
            if(new String(txtNewPass.getPassword()).equals(new String(txtPassConfirm.getPassword()))){
                this.usuario.setPass(new String(txtNewPass.getPassword()));
                modelo.modificarUsuario(this.usuario);
                JOptionPane.showMessageDialog(null, "La contraseña ha sido cambiada", "Cambio Realizado Correctamente", JOptionPane.INFORMATION_MESSAGE);
                dispose();
            }
        }
        else{
            JOptionPane.showMessageDialog(null, "La contraseña introducida no es correcta, revisela", "Error al cambiar", JOptionPane.ERROR_MESSAGE);
        }

    }

    private void onCancel() {
        dispose();
    }

}
