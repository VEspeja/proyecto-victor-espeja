package MVC;

import base.*;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

public class Controlador implements ListSelectionListener, ActionListener {
    SplashScreen splashScreen;
    VistaAcceso vistaAcceso;
    Vista vista;
    Modelo modelo;
    VistaGestionUsuarios vistaGestionUsuarios;
    Usuario usuario;
    Taller taller;
    Mecanico mecanico;
    Reparacion reparacion;
    Vehiculo vehiculo;
    Cliente cliente;
    String nombreUsuario;
    String idiomaActivo;
    VistaCambioPassword vistaCambioPassword;
    JDialogBusqueda busqueda;
    JDialogAsignarMecanicos dialogAsignarMecanicos;
    boolean passFail;
    boolean userFail;
    String rutaImagenSeleccionada;
    private Properties selectorIdioma;
    Actualizar hiloActualizar;
    Connection conexion1 = null;
    JasperReport report1 = null;
    Map<String,Object> parametros;
    JasperPrint jasperPrint;
    JasperViewer jv;

    boolean registradoTelf;
    boolean registradoDni;
    boolean registrado;
    int completo = 0;
    String campoRequerido = null;


    public Controlador(VistaAcceso vistaAcceso, Vista vista, VistaGestionUsuarios vistaGestionUsuarios, Modelo modelo) {
        this.modelo = modelo;
        splashScreen = new SplashScreen(modelo);
        splashScreen.run();
        //modelo.conectar();
        this.vista = vista;
        this.vistaGestionUsuarios = vistaGestionUsuarios;
        selectorIdioma = new Properties();
        userFail = true;
        passFail = false;
        this.vistaAcceso = vistaAcceso;
        anyadirActionListener(this);
        anyadirListSelectionListener(this);
        if (vistaAcceso.dcbmIdioma.getSelectedItem().toString().equals("imagenes/españa.png")) {
            try {
                cambiarIdiomaTextos("spanish");
                idiomaActivo = "spanish";
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        vistaAcceso.frame.setVisible(true);
        hiloActualizar = new Actualizar();
        hiloActualizar.start();
        registradoTelf = false;
        registradoDni = false;
        registrado = false;

    }

    public Controlador() {
    }


    private void anyadirActionListener(ActionListener listener) {
        vistaAcceso.btnAcceder.addActionListener(listener);
        vistaAcceso.cbIdioma.addActionListener(listener);
        vistaGestionUsuarios.btnRegistrar.addActionListener(listener);
        vistaGestionUsuarios.btnModificar.addActionListener(listener);
        vistaGestionUsuarios.btnEliminar.addActionListener(listener);
        vista.btnAltaTaller.addActionListener(listener);
        vista.btnModificarTaller.addActionListener(listener);
        vista.btnEliminarTaller.addActionListener(listener);
        vista.btnRegistrarMecanico.addActionListener(listener);
        vista.btnModificarMecanico.addActionListener(listener);
        vista.btnEliminarMecanico.addActionListener(listener);
        vista.btnRegistrarReparacion.addActionListener(listener);
        vista.btnModificarReparacion.addActionListener(listener);
        vista.btnEliminarReparacion.addActionListener(listener);
        vista.btnRegistrarCliente.addActionListener(listener);
        vista.btnModificarCliente.addActionListener(listener);
        vista.btnEliminarCliente.addActionListener(listener);
        vista.btnRegistrarVehiculo.addActionListener(listener);
        vista.btnModificarVehiculo.addActionListener(listener);
        vista.btnEliminarVehiculo.addActionListener(listener);
        vista.btnSeleccionarImagen.addActionListener(listener);
        vista.chEspecialidadMecanico.addActionListener(listener);
        vista.btnAsignarMecanicos.addActionListener(listener);
        vista.btnBuscarTalleres.addActionListener(listener);
        vista.btnBuscarMecanicos.addActionListener(listener);
        vista.btnBuscarReparaciones.addActionListener(listener);
        vista.btnBuscarVehiculos.addActionListener(listener);
        vista.btnBuscarClientes.addActionListener(listener);
        vista.btnInformeReparacionesAnyo.addActionListener(listener);
        vista.btnInformeGrafico.addActionListener(listener);
        vista.btnInformeClientes.addActionListener(listener);


        vista.itemSalir.addActionListener(listener);
        vista.itemAdmin.addActionListener(listener);
        vista.itemContrasenya.addActionListener(listener);
    }

    private void anyadirListSelectionListener(ListSelectionListener listener) {
        vistaGestionUsuarios.listaUsuarios.addListSelectionListener(listener);
        vista.listaClientes.addListSelectionListener(listener);
        vista.listaMecanicos.addListSelectionListener(listener);
        vista.listaTalleres.addListSelectionListener(listener);
        vista.listaReparaciones.addListSelectionListener(listener);
        vista.listaVehiculos.addListSelectionListener(listener);
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (e.getValueIsAdjusting()) {
            if (e.getSource() == vista.listaTalleres && vista.listaTalleres.getSelectedValue() != null) {
                taller = (Taller) vista.listaTalleres.getSelectedValue();
                mostrarTaller(taller);
            } else if (e.getSource() == vista.listaMecanicos && vista.listaMecanicos.getSelectedValue() != null) {
                mecanico = (Mecanico) vista.listaMecanicos.getSelectedValue();
                mostrarMecanico(mecanico);
            } else if (e.getSource() == vista.listaClientes && vista.listaClientes.getSelectedValue() != null) {
                cliente = (Cliente) vista.listaClientes.getSelectedValue();
                mostrarCliente(cliente);
            } else if (e.getSource() == vistaGestionUsuarios.listaUsuarios && vistaGestionUsuarios.listaUsuarios.getSelectedValue() != null) {
                usuario = (Usuario) vistaGestionUsuarios.listaUsuarios.getSelectedValue();
                vistaGestionUsuarios.txtPass.setEnabled(false);
                mostrarUsuario(usuario);
            } else if (e.getSource() == vista.listaReparaciones && vista.listaReparaciones.getSelectedValue() != null) {
                reparacion = (Reparacion) vista.listaReparaciones.getSelectedValue();
                mostrarReparacion(reparacion);
                //listarJugadoresEntrenador(entrenador);
                //listarJugadoresDisponiblesParaEntrenador(entrenador);
            } else if (e.getSource() == vista.listaVehiculos && vista.listaVehiculos.getSelectedValue() != null) {
                vehiculo = (Vehiculo) vista.listaVehiculos.getSelectedValue();
                mostrarVehiculo(vehiculo);
                //listarEntrenadoresJugador(jugador);
                //listarEntrenadoresDisponiblesParaJugador(jugador);

            }
            if (vista.listaTalleres.getSelectedValue() == null) {
                taller = null;
                vaciarDatosTaller();
            }
            if (vista.listaMecanicos.getSelectedValue() == null) {
                mecanico = null;
                vaciarDatosMecanico();
            }
            if (vista.listaReparaciones.getSelectedValue() == null) {
                reparacion = null;
                vaciarDatosReparacion();
            }
            if (vista.listaVehiculos.getSelectedValue() == null) {
                vehiculo = null;
                vaciarDatosVehiculo();
            }
            if (vista.listaClientes.getSelectedValue() == null) {
                cliente = null;
                vaciarDatosCliente();
            }
            if(vistaGestionUsuarios.listaUsuarios.getSelectedValue() == null){
                usuario = null;
                vistaGestionUsuarios.txtPass.setEnabled(true);
                vaciarDatosUsuario();
            }
        }
    }

    private void mostrarUsuario(Usuario usuario) {
        vistaGestionUsuarios.txtUsuario.setText(usuario.getNameUser());
        if (usuario.getRol() == 2) {
            vistaGestionUsuarios.dcbmRol.setSelectedItem("Empleado");
        } else {
            vistaGestionUsuarios.dcbmRol.setSelectedItem("Consultor");
        }
    }

    private void mostrarReparacion(Reparacion reparacion) {
        vista.txtIdReparacion.setText("" + reparacion.getId());
        vista.txtCosteReparacion.setText("" + reparacion.getCoste());
        vista.taDiagnosticoReparacion.setText(reparacion.getDiagnostico().toUpperCase());
        vista.dtEntregaReparacion.setDate(reparacion.getFechaFinalizacion().toLocalDate());
        vista.dtEmisionReparacion.setDate(reparacion.getFechaEmision().toLocalDate());
        vista.dcbmMatriculaReparacion.setSelectedItem(reparacion.getVehiculo().obtenerDatos());
    }


    private void mostrarVehiculo(Vehiculo vehiculo) {
        vista.txtIdVehiculo.setText("" + vehiculo.getId());
        vista.txtMatriculaVehiculo.setText(vehiculo.getMatricula().toUpperCase());
        vista.dcbmMarcaVehiculo.setSelectedItem(vehiculo.getMarca());
        vista.dcbmCombustibleVehiculo.setSelectedItem(vehiculo.getCombustible());
        vista.dcbmClienteVehiculo.setSelectedItem(vehiculo.getCliente());
        vista.txtModeloVehiculo.setText(vehiculo.getModelo().toUpperCase());
        vista.dtMatriculacionVehiculo.setDate(vehiculo.getFechaMatriculacion().toLocalDate());
        Image foto1 = new ImageIcon(vehiculo.getImagen()).getImage();
        ImageIcon imagen = new ImageIcon(foto1);
        System.out.println(imagen.getIconHeight() + imagen.getIconWidth());
        if(imagen.getIconWidth() >= 200 || imagen.getIconHeight() >= 100) {
            vista.etImagenVehiculo.setIcon(new ImageIcon(foto1.getScaledInstance(150, 100, Image.SCALE_DEFAULT)));
        }else{
            vista.etImagenVehiculo.setIcon(imagen);
        }
        vista.txtNChasisVehiculo.setText(vehiculo.getNumeroChasis());
    }

    private void mostrarCliente(Cliente cliente) {
        vista.txtIdCliente.setText("" + cliente.getId());
        vista.txtNombreCliente.setText(cliente.getNombre().toUpperCase());
        vista.txtApellidosCliente.setText(cliente.getApellidos().toUpperCase());
        vista.txtDomicilioCliente.setText(cliente.getDomicilio().toUpperCase());
        vista.txtDniCliente.setText(cliente.getDni().toUpperCase());
        vista.txtCpCliente.setText(cliente.getCodigoPostal());
        vista.txtTelefonoCliente.setText("" + cliente.getTelefono());
        vista.dtFechaNacimientoCliente.setDate(cliente.getFechaNacimiento().toLocalDate());
        if(cliente.isSocio()){
            vista.chSocioCliente.setSelected(true);
        }
        else{
            vista.chSocioCliente.setSelected(false);
        }
    }

    private void mostrarMecanico(Mecanico mecanico) {
        vista.txtIdMecanico.setText("" + mecanico.getId());
        vista.txtNombreMecanico.setText(mecanico.getNombre().toUpperCase());
        vista.txtApellidosMecanico.setText(mecanico.getApellidos().toUpperCase());
        vista.txtDniMecanico.setText(mecanico.getDni().toUpperCase());
        if (mecanico.getEspecialidad().equals("")) {
            vista.chEspecialidadMecanico.setSelected(false);
            vista.txtEspecialidadMecanico.setText("");
            vista.txtEspecialidadMecanico.setVisible(false);
        } else {
            vista.chEspecialidadMecanico.setSelected(true);
            vista.txtEspecialidadMecanico.setVisible(true);
            vista.txtEspecialidadMecanico.setText(mecanico.getEspecialidad().toUpperCase());
        }
        vista.txtExperienciaMecanico.setText("" + mecanico.getAnyosExperiencia());
        vista.txtNEmpleadoMecanico.setText(mecanico.getNumeroEmpleado());
        vista.dtIncorporacionMecanico.setDate(mecanico.getFechaIncorporacion().toLocalDate());
        vista.dcbmTallerMecanico.setSelectedItem(mecanico.getTaller());


    }

    private void mostrarTaller(Taller taller) {
        vista.txtIdTaller.setText("" + taller.getId());
        vista.txtNombreTaller.setText("" + taller.getNombre().toUpperCase());
        vista.txtUbicacionTaller.setText("" + taller.getUbicacion().toUpperCase());
        vista.txtTelefonoTaller.setText("" + taller.getTelefono());
        vista.dtFechaAperturaTaller.setDate(LocalDate.parse("" + taller.getFechaApertura()));
        vista.txtHorarioAperturaTaller.setText("" + taller.getHorarioApertura());
        vista.txtHorarioCierreTaller.setText("" + taller.getHorarioCierre());
        if (taller.getTiendaOnline()) {
            vista.chTiendaOnlineTaller.setSelected(true);
        } else {
            vista.chTiendaOnlineTaller.setSelected(false);
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case "Cambio Idioma":
                if (vistaAcceso.dcbmIdioma.getSelectedItem().toString().equals("imagenes/english.png") &&
                        !vistaAcceso.etUsuarioLogin.getText().equals("User")) {
                    try {
                        cambiarIdiomaTextos("english");
                        idiomaActivo = "english";
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                } else {
                    try {
                        cambiarIdiomaTextos("spanish");
                        idiomaActivo = "spanish";
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                }
                break;
            case "Acceder":
                userFail = true;
                if (!(vistaAcceso.txtUsuario.getText().isEmpty() || vistaAcceso.txtUsuario.getText().equals(" ")) && !(vistaAcceso.txtContrasenya.getText().isEmpty() || vistaAcceso.txtContrasenya.getText().equals(" "))) {
                    for (Usuario usuario : modelo.getUsuarios()) {
                        if (vistaAcceso.txtUsuario.getText().equals(usuario.getNameUser())) {
                            userFail = false;
                            passFail = false;
                            if (vistaAcceso.txtContrasenya.getText().equals(usuario.getPass())) {
                                vista.frame.setVisible(true);
                                vistaAcceso.frame.dispose();
                                nombreUsuario = usuario.getNameUser();
                                if (usuario.getRol() == 2) {
                                    vista.menuAdmin.setVisible(false);
                                    vista.menuAdmin.setEnabled(false);
                                } else if (usuario.getRol() == 3) {
                                    quitarPermisosConsultor();
                                }
                            } else {
                                passFail = true;
                            }
                        }
                    }
                    if (passFail == true && userFail == false) {
                        JOptionPane.showMessageDialog(null, "La contraseña introducida no es correcta, revisela", "Error al Acceder", JOptionPane.ERROR_MESSAGE);
                    }
                }
                if (userFail == true) {
                    JOptionPane.showMessageDialog(null, "El usuario introducido no es correcto, reviselo, se diferencian las mayusculas", "Error al Acceder", JOptionPane.ERROR_MESSAGE);
                }
                if (vistaAcceso.txtUsuario.getText().isEmpty() || vistaAcceso.txtUsuario.getText().equals(" ")) {
                    JOptionPane.showMessageDialog(null, "Introduzca nombre de usuario, este campo no puede esta vacio", "Error al Acceder", JOptionPane.ERROR_MESSAGE);
                }
                if (vistaAcceso.txtContrasenya.getText().isEmpty()) {
                    JOptionPane.showMessageDialog(null, "Introduzca la contraseña, este campo no puede esta vacio", "Error al Acceder", JOptionPane.ERROR_MESSAGE);
                }
                break;
            case "Regresar":
                vista.frame.dispose();
                vistaAcceso = new VistaAcceso();
                vista = new Vista();
                anyadirActionListener(this);
                anyadirListSelectionListener(this);
                if (idiomaActivo.equals("spanish")) {
                    vistaAcceso.dcbmIdioma.setSelectedItem(vistaAcceso.spanish);

                } else {
                    vistaAcceso.dcbmIdioma.setSelectedItem(vistaAcceso.english);
                }
                try {
                    cambiarIdiomaTextos(idiomaActivo);
                } catch (IOException ex) {
                    ex.printStackTrace();
                }

                vistaAcceso.frame.setVisible(true);
                break;
            case "Gestionar Usuarios":
                vistaGestionUsuarios.frame.setTitle(selectorIdioma.getProperty("ventanaGestionUser"));
                vistaGestionUsuarios.frame.setVisible(true);
                break;
            case "Cambiar Pass":
                for (Usuario user : modelo.getUsuarios()) {
                    if (user.getNameUser().equals(nombreUsuario)) {
                        vistaCambioPassword = new VistaCambioPassword(modelo, user, selectorIdioma);
                        //modificarContrasenyaUsuario(user);
                    }
                }
                //vistaCambioPass.frame.setVisible(true);
                break;
            case "Registrar Usuario":
                modelo.altaUsuario(vistaGestionUsuarios.txtUsuario.getText(), vistaGestionUsuarios.txtPass.getText(), String.valueOf(vistaGestionUsuarios.dcbmRol.getSelectedItem()));
                vaciarDatosUsuario();
                break;
            case "Modificar Usuario":
                modificarUsuario(usuario);
                modelo.modificarUsuario(usuario);
                vaciarDatosUsuario();
                break;
            case "Eliminar Usuario":
                modelo.eliminarUsuario(usuario);
                vaciarDatosUsuario();
                break;

            case "Alta Taller":
                if (vista.chTiendaOnlineTaller.isSelected()) {
                    modelo.altaTaller(vista.txtNombreTaller.getText().toUpperCase(), vista.txtUbicacionTaller.getText().toUpperCase(), Integer.parseInt(vista.txtTelefonoTaller.getText()),
                            vista.dtFechaAperturaTaller.getDate(), vista.txtHorarioAperturaTaller.getText(), vista.txtHorarioCierreTaller.getText(), true);
                } else {
                    modelo.altaTaller(vista.txtNombreTaller.getText().toUpperCase(), vista.txtUbicacionTaller.getText().toUpperCase(), Integer.parseInt(vista.txtTelefonoTaller.getText()),
                            vista.dtFechaAperturaTaller.getDate(), vista.txtHorarioAperturaTaller.getText(), vista.txtHorarioCierreTaller.getText(), false);
                }
                vaciarDatosTaller();
                break;
            case "Modificar Taller":
                modificarTaller(taller);
                vaciarDatosTaller();
                break;
            case "Eliminar Taller":
                modelo.eliminarTaller(taller);
                vaciarDatosTaller();
                break;
            case "Especialidad":
                comprobarChEspecialidad();
                break;
            case "Registrar Mecanico":
                comprobarYRegistrarMecanico();
                break;
            case "Modificar Mecanico":
                modificarMecanico(mecanico);
                vaciarDatosMecanico();
                break;
            case "Eliminar Mecanico":
                mecanico.desvincularTodos();
                modelo.eliminarMecanico(mecanico);
                vaciarDatosMecanico();
                break;

            case "Registrar Reparacion":
                for (Vehiculo vehiculo : modelo.getVehiculos()) {
                    if (vehiculo.obtenerDatos().equals(vista.dcbmMatriculaReparacion.getSelectedItem().toString())) {
                        modelo.altaReparacion(vista.dtEmisionReparacion.getDate(), Double.parseDouble(vista.txtCosteReparacion.getText()), vista.dtEntregaReparacion.getDate(), vista.taDiagnosticoReparacion.getText().toUpperCase(), vehiculo);
                    }
                }
                vaciarDatosReparacion();
                break;
            case "Modificar Reparacion":
                modificarReparacion(reparacion);
                vaciarDatosReparacion();
                break;
            case "Eliminar Reparacion":
                reparacion.desvincularTodos();
                modelo.eliminarReparacion(reparacion);
                vaciarDatosReparacion();
                break;

            case "Asignar Mecanicos":
                if (reparacion != null) {
                    dialogAsignarMecanicos = new JDialogAsignarMecanicos(reparacion, modelo, selectorIdioma);
                } else {
                    JOptionPane.showMessageDialog(null, "Debe seleccionar una reparacion", "Error al Acceder", JOptionPane.ERROR_MESSAGE);
                }
                break;

            case "Seleccionar Imagen":
                JFileChooser selector = new JFileChooser();
                int opt = selector.showSaveDialog(null);
                if (opt == JFileChooser.APPROVE_OPTION) {
                    // se guarda el fichero que ha sido seleccionado
                    File imagenRecogida = selector.getSelectedFile();
                    String nombreImagenRecogida = imagenRecogida.getName();
                    File imagenAlmacenada = new File("imagenes/vehiculos/" + nombreImagenRecogida);
                    try {
                        copiarImagenes(imagenRecogida, imagenAlmacenada);
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }

                    rutaImagenSeleccionada = imagenAlmacenada.getPath();
                    Image foto = new ImageIcon(rutaImagenSeleccionada).getImage();
                    ImageIcon imagen1 = new ImageIcon(foto);
                    if (imagen1.getIconWidth() >= 150 || imagen1.getIconHeight() >= 100) {
                        vista.etImagenVehiculo.setIcon(new ImageIcon(foto.getScaledInstance(150, 100, Image.SCALE_DEFAULT)));
                    } else {
                        vista.etImagenVehiculo.setIcon(imagen1);
                    }
                    vista.etImagenVehiculo.setIcon(new ImageIcon(foto.getScaledInstance(150, 100, Image.SCALE_DEFAULT)));

                }
                break;
            case "Registrar Vehiculo":
                comprobarYRegistrarVehiculo();
                break;
            case "Modificar Vehiculo":
                modificarVehiculo(vehiculo);
                vaciarDatosVehiculo();
                break;
            case "Eliminar Vehiculo":
                modelo.eliminarVehiculo(vehiculo);
                vaciarDatosVehiculo();
                break;
            case "Registrar Cliente":
                comprobarYRegistrarCliente();
                break;
            case "Modificar Cliente":
                modificarCliente(cliente);
                vaciarDatosCliente();
                break;
            case "Eliminar Cliente":
                modelo.eliminarCliente(cliente);
                vaciarDatosCliente();
                break;
            case "Buscar Talleres":
                busqueda = new JDialogBusqueda("Taller", modelo, selectorIdioma);
                break;
            case "Buscar Mecanicos":
                busqueda = new JDialogBusqueda("Mecanico", modelo, selectorIdioma);
                break;
            case "Buscar Reparaciones":
                busqueda = new JDialogBusqueda("Reparacion", modelo, selectorIdioma);
                break;
            case "Buscar Clientes":
                busqueda = new JDialogBusqueda("Cliente", modelo, selectorIdioma);
                break;
            case "Buscar Vehiculos":
                busqueda = new JDialogBusqueda("Vehiculo", modelo, selectorIdioma);
                break;
            case "Informe Reparaciones Anyo":

                try {
                    Class.forName("com.mysql.jdbc.Driver");
                    conexion1 = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/gestion_talleres", "root", "");
                    report1 = (JasperReport) JRLoader.loadObjectFromFile("reports/reportMecanico.jasper");
                    parametros = new HashMap<String, Object>();
                    parametros.put("titulo", "Reparaciones de los mecanicos en " + vista.dcbmAnyoInforme.getSelectedItem().toString());
                    parametros.put("fecha", vista.dcbmAnyoInforme.getSelectedItem().toString());
                    jasperPrint = JasperFillManager.fillReport(report1, parametros, conexion1);

                    jv = new JasperViewer(jasperPrint, false);
                    jv.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                    jv.setVisible(true);

                    mostarPdf(jasperPrint, "Informe Reparaciones Mecanico Año" + vista.dcbmAnyoInforme.getSelectedItem().toString());

                } catch (JRException e1) {
                    e1.printStackTrace();
                    System.out.println("Vaya, no se puede cargar el fichero");
                } catch (ClassNotFoundException ex) {
                    ex.printStackTrace();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                break;
            case "Informe Grafico":

                try {
                    Class.forName("com.mysql.jdbc.Driver");
                } catch (ClassNotFoundException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
                try {
                    conexion1 = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/gestion_talleres", "root", "");
                } catch (SQLException e2) {
                    e2.printStackTrace();
                }
                JasperReport report1 = null;
                try {
                    report1 = (JasperReport) JRLoader.loadObjectFromFile("reports/reportGraficoReparacionesTaller.jasper");
                    parametros = new HashMap<String, Object>();
                    parametros.put("titulo", "Cantidad de Reparaciones por Taller");
                    jasperPrint = JasperFillManager.fillReport(report1, parametros, conexion1);

                    jv = new JasperViewer(jasperPrint, false);
                    jv.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                    jv.setVisible(true);

                    mostarPdf(jasperPrint, "Informe grafico reparaciones por taller");

                } catch (JRException e1) {
                    e1.printStackTrace();
                    System.out.println("Vaya, no se puede cargar el fichero");
                }
                break;

            case "Informe Clientes":

                try {
                    Class.forName("com.mysql.jdbc.Driver");
                } catch (ClassNotFoundException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
                try {
                    conexion1 = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/gestion_talleres", "root", "");
                } catch (SQLException e2) {
                    e2.printStackTrace();
                }
                JasperReport report2 = null;
                try {
                    report2 = (JasperReport) JRLoader.loadObjectFromFile("reports/reportClienteVehiculos.jasper");
                    parametros = new HashMap<String, Object>();
                    parametros.put("titulo", "Reparaciones de Clientes y Vehiculos");
                    jasperPrint = JasperFillManager.fillReport(report2, parametros, conexion1);

                    jv = new JasperViewer(jasperPrint, false);
                    jv.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                    jv.setVisible(true);

                    mostarPdf(jasperPrint, "Informe Reparaciones por Cliente");

                } catch (JRException e1) {
                    e1.printStackTrace();
                    System.out.println("Vaya, no se puede cargar el fichero");
                }
                break;

        }
        if (!e.getActionCommand().equals("Seleccionar Imagen")){
            listarClientes();
        listarUsuarios();
        listarClientesComboBox();
        listarCombustiblesComoBox();
        listarMarcasComboBox();
        listarMatriculasComboBox();
        listarTalleresComboBox();
        listarMecanicos();
        listarReparaciones();
        listarTalleres();
        listarVehiculos();
        listarAnyosInforme();
    }

    }

    private void quitarPermisosConsultor() {
        vista.menuAdmin.setVisible(false);
        vista.menuAdmin.setEnabled(false);

        vista.txtNombreTaller.setEditable(false);
        vista.txtUbicacionTaller.setEditable(false);
        vista.dtFechaAperturaTaller.setEnabled(false);
        vista.txtHorarioCierreTaller.setEditable(false);
        vista.txtHorarioAperturaTaller.setEditable(false);
        vista.txtTelefonoTaller.setEditable(false);
        vista.btnAltaTaller.setVisible(false);
        vista.btnModificarTaller.setVisible(false);
        vista.btnEliminarTaller.setVisible(false);

        vista.txtApellidosMecanico.setEditable(false);
        vista.txtNombreMecanico.setEditable(false);
        vista.txtDniMecanico.setEditable(false);
        vista.dtIncorporacionMecanico.setEnabled(false);
        vista.txtExperienciaMecanico.setEditable(false);
        vista.txtNEmpleadoMecanico.setEditable(false);
        vista.txtEspecialidadMecanico.setEditable(false);
        vista.chEspecialidadMecanico.setEnabled(false);
        vista.cbTallerMecanico.setEnabled(false);
        vista.btnRegistrarMecanico.setVisible(false);
        vista.btnModificarMecanico.setVisible(false);
        vista.btnEliminarMecanico.setVisible(false);

        vista.cbMatriculaReparacion.setEnabled(false);
        vista.dtEmisionReparacion.setEnabled(false);
        vista.dtEntregaReparacion.setEnabled(false);
        vista.taDiagnosticoReparacion.setEnabled(false);
        vista.txtCosteReparacion.setEditable(false);
        vista.btnRegistrarReparacion.setVisible(false);
        vista.btnModificarReparacion.setVisible(false);
        vista.btnEliminarReparacion.setVisible(false);
        vista.btnAsignarMecanicos.setVisible(false);

        vista.txtNombreCliente.setEditable(false);
        vista.txtApellidosCliente.setEditable(false);
        vista.txtDniCliente.setEditable(false);
        vista.txtTelefonoCliente.setEditable(false);
        vista.dtFechaNacimientoCliente.setEnabled(false);
        vista.txtDomicilioCliente.setEditable(false);
        vista.txtCpCliente.setEditable(false);
        vista.chSocioCliente.setEnabled(false);
        vista.btnRegistrarCliente.setVisible(false);
        vista.btnModificarCliente.setVisible(false);
        vista.btnEliminarCliente.setVisible(false);


        vista.cbMarcaVehiculo.setEnabled(false);
        vista.cbCombustibleVehiculo.setEnabled(false);
        vista.txtMatriculaVehiculo.setEditable(false);
        vista.txtModeloVehiculo.setEditable(false);
        vista.dtMatriculacionVehiculo.setEnabled(false);
        vista.txtNChasisVehiculo.setEditable(false);
        vista.cbPropietarioVehiculo.setEnabled(false);
        vista.btnSeleccionarImagen.setVisible(false);
        vista.btnRegistrarVehiculo.setVisible(false);
        vista.btnModificarVehiculo.setVisible(false);
        vista.btnEliminarVehiculo.setVisible(false);


    }

    private void comprobarChEspecialidad() {
        if (vista.chEspecialidadMecanico.isSelected()) {
            vista.txtEspecialidadMecanico.setVisible(true);
        } else {
            vista.txtEspecialidadMecanico.setVisible(false);
        }
    }

    private void comprobarYRegistrarCliente() {
        completo = 0;
        registradoTelf = false;
        registradoDni= false;
        for (Cliente cliente : modelo.getClientes()) {
            if (cliente.getDni().equals(vista.txtDniCliente.getText())) {
                registradoTelf = true;
            }
            if (cliente.getTelefono() == Integer.parseInt(vista.txtTelefonoCliente.getText())) {
                registradoDni = true;
            }
        }
        try {
            this.selectorIdioma.load(new FileInputStream(idiomaActivo + ".props"));
            if(vista.txtNombreCliente.getText().equals("")){
                vista.etNombreClienteValor.setVisible(true);
                vista.etNombreClienteValor.setText(campoRequerido);
            }
            if(! vista.txtNombreCliente.getText().equals("")){
                completo++;
                if(!vista.etNombreClienteValor.getText().equals("")) {
                    vista.etNombreClienteValor.setVisible(false);
                }
            }
            if(vista.txtApellidosCliente.getText().equals("")){
                vista.etApellidosClienteValor.setVisible(true);
                vista.etApellidosClienteValor.setText(campoRequerido);
            }
            if(! vista.txtApellidosCliente.getText().equals("")){
                completo++;
                if(!vista.etApellidosClienteValor.getText().equals("")) {
                    vista.etApellidosClienteValor.setVisible(false);
                }
            }
            if(vista.txtDniCliente.getText().equals("")){
                vista.etDniClienteValor.setVisible(true);
                vista.etDniClienteValor.setText(campoRequerido);
            }
            if(! vista.txtDniCliente.getText().equals("")){
                completo++;
                if(!vista.etDniClienteValor.getText().equals("")) {
                    vista.etDniClienteValor.setVisible(false);
                }
                if(registradoDni) {
                    JOptionPane.showMessageDialog(null, "El DNI introducido ya ha sido registrado,reviselo\n Gracias", "Error al Registrar", JOptionPane.ERROR_MESSAGE);
                }
            }
            if(vista.txtTelefonoCliente.getText().equals("")){
                vista.etTelefonoClienteValor.setVisible(true);
                vista.etTelefonoClienteValor.setText(campoRequerido);
            }
            if(! vista.txtTelefonoCliente.getText().equals("")){
                completo++;
                if(!vista.etTelefonoClienteValor.getText().equals("")) {
                    vista.etTelefonoClienteValor.setVisible(false);
                }
                if(registradoTelf) {
                    JOptionPane.showMessageDialog(null, "El Numero de Telefono introducido ya ha sido usado", "Error al Registrar", JOptionPane.ERROR_MESSAGE);
                }
            }
            if(vista.txtDomicilioCliente.getText().equals("")){
                vista.etDomicilioClienteValor.setVisible(true);
                vista.etDomicilioClienteValor.setText(campoRequerido);
            }
            if(! vista.txtDomicilioCliente.getText().equals("")){
                completo++;
                if(!vista.etDomicilioClienteValor.getText().equals("")) {
                    vista.etDomicilioClienteValor.setVisible(false);
                }
            }
            if(vista.dtFechaNacimientoCliente.getText().equals("")){
                vista.etFechaNacimientoClienteValor.setVisible(true);
                vista.etFechaNacimientoClienteValor.setText(campoRequerido);
            }
            if(! vista.dtFechaNacimientoCliente.getText().equals("")){
                completo++;
                if(!vista.etFechaNacimientoClienteValor.getText().equals("")) {
                    vista.etFechaNacimientoClienteValor.setVisible(false);
                }
            }
            if(!registradoTelf && !registradoDni && completo == 6) {
                if (vista.chSocioCliente.isSelected()) {
                    modelo.altaCliente(vista.txtNombreCliente.getText().toUpperCase(), vista.txtApellidosCliente.getText().toUpperCase(), vista.txtDniCliente.getText().toUpperCase(), Integer.parseInt(vista.txtTelefonoCliente.getText()),
                            vista.dtFechaNacimientoCliente.getDate(), vista.txtDomicilioCliente.getText().toUpperCase(), vista.txtCpCliente.getText(), true);
                } else {
                    modelo.altaCliente(vista.txtNombreCliente.getText().toUpperCase(), vista.txtApellidosCliente.getText().toUpperCase(), vista.txtDniCliente.getText().toUpperCase(), Integer.parseInt(vista.txtTelefonoCliente.getText()),
                            vista.dtFechaNacimientoCliente.getDate(), vista.txtDomicilioCliente.getText().toUpperCase(), vista.txtCpCliente.getText(), false);
                }
                vaciarDatosCliente();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private void comprobarYRegistrarMecanico() {
        completo = 0;
        registrado = false;
        for (Mecanico mecanico : modelo.getMecanicos()) {
            if (mecanico.getDni().equals(vista.txtDniMecanico.getText())) {
                registrado = true;
            }

        }
        try {
            this.selectorIdioma.load(new FileInputStream(idiomaActivo + ".props"));
            if(vista.txtNombreMecanico.getText().equals("")){
                vista.etNombreMecanicoValor.setVisible(true);
                vista.etNombreMecanicoValor.setText(campoRequerido);
            }
            if(! vista.txtNombreMecanico.getText().equals("")){
                completo++;
                if(!vista.etNombreMecanicoValor.getText().equals("")) {
                    vista.etNombreMecanicoValor.setVisible(false);
                }
            }
            if(vista.txtApellidosMecanico.getText().equals("")){
                vista.etApellidosMecanicoValor.setVisible(true);
                vista.etApellidosMecanicoValor.setText(campoRequerido);
            }
            if(! vista.txtApellidosMecanico.getText().equals("")){
                completo++;
                if(!vista.etApellidosMecanicoValor.getText().equals("")) {
                    vista.etApellidosMecanicoValor.setVisible(false);
                }
            }
            if(vista.txtDniMecanico.getText().equals("")){
                vista.etDniMecanicoValor.setVisible(true);
                vista.etDniMecanicoValor.setText(campoRequerido);
            }
            if(! vista.txtDniMecanico.getText().equals("")){
                completo++;
                if(!vista.etDniMecanicoValor.getText().equals("")) {
                    vista.etDniMecanicoValor.setVisible(false);
                }
                if (registrado) {
                    JOptionPane.showMessageDialog(null,"Ya existe un mecanico asignado con ese DNI, reviselo por favor.","Error al Registrar",JOptionPane.ERROR_MESSAGE);
                }
            }
            if(vista.txtNEmpleadoMecanico.getText().equals("")){
                vista.etNEmpleadoMecanicoValor.setVisible(true);
                vista.etNEmpleadoMecanicoValor.setText(campoRequerido);
            }
            if(! vista.txtNEmpleadoMecanico.getText().equals("")){
                completo++;
                if(!vista.etNEmpleadoMecanicoValor.getText().equals("")) {
                    vista.etNEmpleadoMecanicoValor.setVisible(false);
                }
            }
            if(vista.txtExperienciaMecanico.getText().equals("")){
                vista.etExperienciaMecanicoValor.setVisible(true);
                vista.etExperienciaMecanicoValor.setText(campoRequerido);
            }
            if(! vista.txtExperienciaMecanico.getText().equals("")){
                completo++;
                if(!vista.etExperienciaMecanicoValor.getText().equals("")) {
                    vista.etExperienciaMecanicoValor.setVisible(false);
                }
            }
            if(vista.dtIncorporacionMecanico.getText().equals("")){
                vista.etFechaIncorporacionMecanicoValor.setVisible(true);
                vista.etFechaIncorporacionMecanicoValor.setText(campoRequerido);
            }
            if(! vista.dtIncorporacionMecanico.getText().equals("")){
                completo++;
                if(!vista.etFechaIncorporacionMecanicoValor.getText().equals("")) {
                    vista.etFechaIncorporacionMecanicoValor.setVisible(false);
                }
            }
            if(vista.dcbmTallerMecanico.getSelectedItem() == null){
                vista.etTallerMecanicoValor.setVisible(true);
                vista.etTallerMecanicoValor.setText(campoRequerido);
            }
            if(vista.dcbmTallerMecanico.getSelectedItem() != null){
                completo ++;
                if(!vista.etTallerMecanicoValor.getText().equals("")) {
                    vista.etTallerMecanicoValor.setVisible(false);
                }
            }
            if(!registrado && completo == 7){
                System.out.println(registrado);
                System.out.println("entro");
               if (vista.chEspecialidadMecanico.isSelected() && !vista.txtEspecialidadMecanico.equals("")) {
                    modelo.altaMecanico(vista.txtNombreMecanico.getText().toUpperCase(), vista.txtApellidosMecanico.getText().toUpperCase(), vista.txtDniMecanico.getText().toUpperCase(), vista.txtNEmpleadoMecanico.getText(), vista.dtIncorporacionMecanico.getDate(),
                            Integer.parseInt(vista.txtExperienciaMecanico.getText()), vista.txtEspecialidadMecanico.getText().toUpperCase(), (Taller) vista.dcbmTallerMecanico.getSelectedItem());
                } else {
                    modelo.altaMecanico(vista.txtNombreMecanico.getText().toUpperCase(), vista.txtApellidosMecanico.getText().toUpperCase(), vista.txtDniMecanico.getText().toUpperCase(), vista.txtNEmpleadoMecanico.getText().toUpperCase(), vista.dtIncorporacionMecanico.getDate(),
                            Integer.parseInt(vista.txtExperienciaMecanico.getText()), "", (Taller) vista.dcbmTallerMecanico.getSelectedItem());
                }
                vaciarDatosMecanico();
            }

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
    private void comprobarYRegistrarVehiculo() {
        completo = 0;
    for (Vehiculo vehiculo : modelo.getVehiculos()) {
        if(vehiculo.getMatricula().equals(vista.txtMatriculaVehiculo.getText())){
            registrado= true;
        }
                    
    }
        try {
            this.selectorIdioma.load(new FileInputStream(idiomaActivo + ".props"));
            if(vista.txtNChasisVehiculo.getText().equals("")){
                vista.etNChasisVehiculoValor.setVisible(true);
                vista.etNChasisVehiculoValor.setText(campoRequerido);
            }
            if(! vista.txtNChasisVehiculo.getText().equals("")){
                completo++;
                if(!vista.etNChasisVehiculoValor.getText().equals("")) {
                    vista.etNChasisVehiculoValor.setVisible(false);
                }
            }
            if(vista.dcbmMarcaVehiculo.getSelectedItem() == null){
                vista.etMarcaVehiculoValor.setVisible(true);
                vista.etMarcaVehiculoValor.setText(campoRequerido);
            }
            if(vista.dcbmMarcaVehiculo.getSelectedItem() != null){
                completo++;
                if(!vista.etMarcaVehiculoValor.getText().equals("")) {
                    vista.etMarcaVehiculoValor.setVisible(false);
                }
            }
            if(vista.txtModeloVehiculo.getText().equals("")){
                vista.etModeloVehiculoValor.setVisible(true);
                vista.etModeloVehiculoValor.setText(campoRequerido);
            }
            if(! vista.txtModeloVehiculo.getText().equals("")){
                completo++;
                if(!vista.etModeloVehiculoValor.getText().equals("")) {
                    vista.etModeloVehiculoValor.setVisible(false);
                }
            }
            if(vista.txtMatriculaVehiculo.getText().equals("")){
                vista.etMatriculaVehiculoValor.setVisible(true);
                vista.etMatriculaVehiculoValor.setText(campoRequerido);
            }
            if(!vista.txtMatriculaVehiculo.getText().equals("")){
                completo++;
                if(!vista.etMatriculaVehiculoValor.getText().equals("") ){
                    vista.etMatriculaVehiculoValor.setVisible(false);
                }
                if(registrado){
                    JOptionPane.showMessageDialog(null,"Ya existe esa matricula asignada a otro vehiculo","Error al Registrar",JOptionPane.ERROR_MESSAGE);
                    registrado= false;
                }
            }
            if(vista.dcbmCombustibleVehiculo.getSelectedItem() == null){
                vista.etCombustibleVehiculoValor.setVisible(true);
                vista.etCombustibleVehiculoValor.setText(campoRequerido);
            }
            if(vista.dcbmCombustibleVehiculo.getSelectedItem() != null){
                completo++;
                if(!vista.etCombustibleVehiculoValor.getText().equals("")) {
                    vista.etCombustibleVehiculoValor.setVisible(false);
                }
            }
            if(vista.dcbmClienteVehiculo.getSelectedItem() == null){
                vista.etPropietarioVehiculoValor.setVisible(true);
                vista.etPropietarioVehiculoValor.setText(campoRequerido);
            }
            if(vista.dcbmClienteVehiculo.getSelectedItem() != null){
                completo++;
                if(!vista.etPropietarioVehiculoValor.getText().equals("")){
                    vista.etPropietarioVehiculoValor.setVisible(false);
                }
            }
            if(vista.dtMatriculacionVehiculo.getText().equals("")){
                vista.etFechaMatriculacionVehiculoValor.setVisible(true);
                vista.etFechaMatriculacionVehiculoValor.setText(campoRequerido);
            }
            if(! vista.dtMatriculacionVehiculo.getText().equals("")){
                completo++;
                if(!vista.etFechaMatriculacionVehiculoValor.getText().equals("")) {
                    vista.etFechaMatriculacionVehiculoValor.setVisible(false);
                }
            }
            if( completo == 7 && !registrado){
                if (vista.etImagenVehiculo.getIcon() == null) {
                    modelo.altaVehiculo((modelo.getVehiculos().size() + 1), vista.txtModeloVehiculo.getText().toUpperCase(), vista.dcbmMarcaVehiculo.getSelectedItem().toString(), vista.txtMatriculaVehiculo.getText(), vista.dcbmCombustibleVehiculo.getSelectedItem().toString(), vista.dtMatriculacionVehiculo.getDate(),
                            vista.txtNChasisVehiculo.getText(), null, (Cliente) vista.dcbmClienteVehiculo.getSelectedItem());
                } else {
                    modelo.altaVehiculo((modelo.getVehiculos().size() + 1), vista.txtModeloVehiculo.getText().toUpperCase(), vista.dcbmMarcaVehiculo.getSelectedItem().toString(), vista.txtMatriculaVehiculo.getText(), vista.dcbmCombustibleVehiculo.getSelectedItem().toString(), vista.dtMatriculacionVehiculo.getDate(),
                            vista.txtNChasisVehiculo.getText(), rutaImagenSeleccionada, (Cliente) vista.dcbmClienteVehiculo.getSelectedItem());
                }
                vaciarDatosVehiculo();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private void mostarPdf(JasperPrint jasperPrint,String nameFile) throws JRException {
        int respuesta = JOptionPane.showConfirmDialog(null, "¿Desea generar un PDF?", "Creacion de PDF", JOptionPane.YES_NO_OPTION);
        if (respuesta == 0) {
            JRExporter exporter = new JRPdfExporter();
            exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
            exporter.setParameter(JRExporterParameter.OUTPUT_FILE, new File(nameFile+".pdf"));
            exporter.exportReport();
        }
    }

    private void listarAnyosInforme() {
        vista.dcbmAnyoInforme.removeAllElements();
        for (Integer anyo: modelo.recogerAnyosReparaciones()){
            vista.dcbmAnyoInforme.addElement(""+anyo);
        }
    }

    private void modificarCliente(Cliente cliente) {
        cliente.setNombre(vista.txtNombreCliente.getText().toUpperCase());
        cliente.setApellidos(vista.txtApellidosCliente.getText().toUpperCase());
        cliente.setCodigoPostal(vista.txtCpCliente.getText());
        cliente.setDni(vista.txtDniCliente.getText().toUpperCase());
        cliente.setDomicilio(vista.txtDomicilioCliente.getText().toUpperCase());
        cliente.setFechaNacimiento(Date.valueOf(vista.dtFechaNacimientoCliente.getDate()));
        cliente.setTelefono(Integer.parseInt(vista.txtTelefonoCliente.getText()));
        if (vista.chSocioCliente.isSelected()) {
            cliente.setSocio(true);
        } else {
            cliente.setSocio(false);
        }
        modelo.modificarCliente(cliente);

    }

    private void modificarVehiculo(Vehiculo vehiculo) {
        vehiculo.setMarca(vista.cbMarcaVehiculo.getSelectedItem().toString());
        vehiculo.setModelo(vista.txtModeloVehiculo.getText().toUpperCase());
        vehiculo.setCombustible(vista.dcbmCombustibleVehiculo.getSelectedItem().toString());
        vehiculo.setFechaMatriculacion(Date.valueOf(vista.dtMatriculacionVehiculo.getDate()));
        vehiculo.setMatricula(vista.txtMatriculaVehiculo.getText().toUpperCase());
        vehiculo.setImagen(vista.etImagenVehiculo.getIcon().toString());
        vehiculo.setNumeroChasis(vista.txtNChasisVehiculo.getText());
        vehiculo.setCliente((Cliente) vista.dcbmClienteVehiculo.getSelectedItem());
        modelo.modificarVehiculo(vehiculo);
    }

    private static void copiarImagenes(File source, File dest)
            throws IOException {
        Files.copy(source.toPath(), dest.toPath(), REPLACE_EXISTING);
    }

    private void modificarReparacion(Reparacion reparacion) {
        reparacion.setCoste(Double.parseDouble(vista.txtCosteReparacion.getText()));
        reparacion.setFechaEmision(Date.valueOf(vista.dtEmisionReparacion.getDate()));
        reparacion.setFechaFinalizacion(Date.valueOf(vista.dtEntregaReparacion.getDate()));
        reparacion.setDiagnostico(vista.taDiagnosticoReparacion.getText().toUpperCase());
        for (Vehiculo vehiculo : modelo.getVehiculos()) {
            if (vehiculo.getMatricula().equals(vista.dcbmMatriculaReparacion.getSelectedItem().toString())) {
                reparacion.setVehiculo(vehiculo);
            }
        }

        modelo.modificarReparacion(reparacion);
    }

    private void modificarMecanico(Mecanico mecanico) {
        mecanico.setNombre(vista.txtNombreMecanico.getText().toUpperCase());
        mecanico.setApellidos(vista.txtApellidosMecanico.getText().toUpperCase());
        mecanico.setDni(vista.txtDniMecanico.getText().toUpperCase());
        mecanico.setNumeroEmpleado(vista.txtNEmpleadoMecanico.getText());
        mecanico.setAnyosExperiencia(Integer.parseInt(vista.txtExperienciaMecanico.getText()));
        if (vista.chEspecialidadMecanico.isSelected() && !vista.txtEspecialidadMecanico.equals("")) {
            mecanico.setEspecialidad(vista.txtEspecialidadMecanico.getText().toUpperCase());
        } else {
            mecanico.setEspecialidad("");
        }
        mecanico.setFechaIncorporacion(Date.valueOf(vista.dtIncorporacionMecanico.getDate()));
        mecanico.setTaller((Taller) vista.dcbmTallerMecanico.getSelectedItem());

        modelo.modificarMecanico(mecanico);
    }

    private void modificarTaller(Taller taller) {
        taller.setNombre(vista.txtNombreTaller.getText().toUpperCase());
        taller.setUbicacion(vista.txtUbicacionTaller.getText().toUpperCase());
        taller.setTelefono(Integer.parseInt(vista.txtTelefonoTaller.getText()));
        taller.setFechaApertura(Date.valueOf(vista.dtFechaAperturaTaller.getDate()));
        taller.setHorarioApertura(vista.txtHorarioAperturaTaller.getText());
        taller.setHorarioCierre(vista.txtHorarioCierreTaller.getText());
        if (vista.chTiendaOnlineTaller.isSelected()) {
            taller.setTiendaOnline(true);
        } else {
            taller.setTiendaOnline(false);
        }

        modelo.modificarTaller(taller);

    }


    private void modificarUsuario(Usuario usuario) {
        usuario.setNameUser(vistaGestionUsuarios.txtUsuario.getText());
        if (vistaGestionUsuarios.dcbmRol.getSelectedItem().toString().equals("Empleado")) {
            usuario.setRol(2);
        } else {
            usuario.setRol(3);
        }
    }

    private void vaciarDatosUsuario() {
        vistaGestionUsuarios.txtUsuario.setText("");
        vistaGestionUsuarios.txtPass.setText("");
        vistaGestionUsuarios.dcbmRol.setSelectedItem(null);
    }

    private void vaciarDatosVehiculo() {
        vista.txtIdVehiculo.setText("");
        vista.dcbmMarcaVehiculo.setSelectedItem(null);
        vista.dcbmCombustibleVehiculo.setSelectedItem(null);
        vista.dtMatriculacionVehiculo.setDate(null);
        vista.txtModeloVehiculo.setText("");
        vista.txtMatriculaVehiculo.setText("");
        vista.etImagenVehiculo.setIcon(null);
        vista.txtNChasisVehiculo.setText("");
        vista.dcbmClienteVehiculo.setSelectedItem(null);
    }

    private void vaciarDatosCliente() {
        vista.txtIdCliente.setText("");
        vista.txtNombreCliente.setText("");
        vista.txtApellidosCliente.setText("");
        vista.txtDniCliente.setText("");
        vista.txtTelefonoCliente.setText("");
        vista.dtFechaNacimientoCliente.setDate(null);
        vista.txtDomicilioCliente.setText("");
        vista.txtCpCliente.setText("");
        vista.chSocioCliente.setSelected(false);
    }

    private void vaciarDatosTaller() {
        vista.txtUbicacionTaller.setText("");
        vista.txtTelefonoTaller.setText("");
        vista.txtIdTaller.setText("");
        vista.txtNombreTaller.setText("");
        vista.dtFechaAperturaTaller.setDate(null);
        vista.txtHorarioCierreTaller.setText("");
        vista.txtHorarioAperturaTaller.setText("");
        vista.chTiendaOnlineTaller.setSelected(false);
    }

    private void vaciarDatosMecanico() {
        vista.txtIdMecanico.setText("");
        vista.txtNombreMecanico.setText("");
        vista.txtApellidosMecanico.setText("");
        vista.txtEspecialidadMecanico.setText("");
        vista.txtExperienciaMecanico.setText("");
        vista.txtDniMecanico.setText("");
        vista.txtNEmpleadoMecanico.setText("");
        vista.dtIncorporacionMecanico.setDate(null);
        vista.dcbmTallerMecanico.setSelectedItem(null);
        vista.chEspecialidadMecanico.setSelected(false);
        comprobarChEspecialidad();
    }

    private void vaciarDatosReparacion() {
        vista.txtCosteReparacion.setText("");
        vista.txtIdReparacion.setText("");
        vista.taDiagnosticoReparacion.setText("");
        vista.dtEmisionReparacion.setDate(null);
        vista.dtEntregaReparacion.setDate(null);
        vista.dcbmMatriculaReparacion.setSelectedItem(null);
    }

    private void listarTalleresComboBox() {
        vista.dcbmTallerMecanico.removeAllElements();
        vista.dcbmTallerMecanico.addElement(null);
        for (Taller taller : modelo.getTalleres()) {
            vista.dcbmTallerMecanico.addElement(taller);
        }
    }

    private void listarTalleresComboBoxHilo() {
        Object ultimo = vista.dcbmTallerMecanico.getSelectedItem();
        listarTalleresComboBox();
        if (mecanico != null) {
            vista.dcbmTallerMecanico.setSelectedItem(mecanico.getTaller());
        } else{
            vista.dcbmTallerMecanico.setSelectedItem(ultimo);
        }
    }

    private void listarClientesComboBox() {
        vista.dcbmClienteVehiculo.removeAllElements();
        vista.dcbmClienteVehiculo.addElement(null);
        for (Cliente cliente : modelo.getClientes()) {
            vista.dcbmClienteVehiculo.addElement(cliente);
        }
    }

    private void listarClientesComboBoxHilo() {
        Object ultimo = vista.dcbmClienteVehiculo.getSelectedItem();
        listarClientesComboBox();
        if (vehiculo != null) {
            vista.dcbmClienteVehiculo.setSelectedItem(vehiculo.getCliente());
        }
        else{
            vista.dcbmClienteVehiculo.setSelectedItem(ultimo);
        }
    }

    private void listarMatriculasComboBox() {
        vista.dcbmMatriculaReparacion.removeAllElements();
        vista.dcbmMatriculaReparacion.addElement(null);
        for (Vehiculo vehiculo : modelo.getVehiculos()) {
            vista.dcbmMatriculaReparacion.addElement(vehiculo.obtenerDatos());
        }
    }

    private void listarMatriculasComboBoxHilo() {
        Object ultimo = vista.dcbmMatriculaReparacion.getSelectedItem();
        listarMatriculasComboBox();
        if (vehiculo != null) {
            vista.dcbmMatriculaReparacion.setSelectedItem(vehiculo.obtenerDatos());
        }
        else{
            vista.dcbmMatriculaReparacion.setSelectedItem(ultimo);
        }
    }

    private void listarCombustiblesComoBox() {
        vista.dcbmCombustibleVehiculo.removeAllElements();
        vista.dcbmCombustibleVehiculo.addElement(null);
        vista.dcbmCombustibleVehiculo.addElement("Gasolina");
        vista.dcbmCombustibleVehiculo.addElement("Diesel");
        vista.dcbmCombustibleVehiculo.addElement("Electrico");
    }

    private void listarCombustiblesComoBoxHilo() {
        Object ultimo = vista.dcbmCombustibleVehiculo.getSelectedItem();
        listarCombustiblesComoBox();
        if (vehiculo != null) {
            vista.dcbmCombustibleVehiculo.setSelectedItem(vehiculo.getCombustible());
        }
        else{
            vista.dcbmCombustibleVehiculo.setSelectedItem(ultimo);
        }
    }

    private void listarMarcasComboBox() {
        vista.dcbmMarcaVehiculo.removeAllElements();
        vista.dcbmMarcaVehiculo.addElement(null);
        vista.dcbmMarcaVehiculo.addElement("Audi");
        vista.dcbmMarcaVehiculo.addElement("Aprillia");
        vista.dcbmMarcaVehiculo.addElement("BMW");
        vista.dcbmMarcaVehiculo.addElement("Citroen");
        vista.dcbmMarcaVehiculo.addElement("Ford");
        vista.dcbmMarcaVehiculo.addElement("Honda");
        vista.dcbmMarcaVehiculo.addElement("Kia");
        vista.dcbmMarcaVehiculo.addElement("Kawasaki");
        vista.dcbmMarcaVehiculo.addElement("Mercedes");
        vista.dcbmMarcaVehiculo.addElement("Nissan");
        vista.dcbmMarcaVehiculo.addElement("Opel");
        vista.dcbmMarcaVehiculo.addElement("Peugeot");
        vista.dcbmMarcaVehiculo.addElement("Renault");
        vista.dcbmMarcaVehiculo.addElement("Seat");
        vista.dcbmMarcaVehiculo.addElement("Suzuki");
        vista.dcbmMarcaVehiculo.addElement("Toyota");
        vista.dcbmMarcaVehiculo.addElement("Volkswagen");
        vista.dcbmMarcaVehiculo.addElement("Yamaha");
    }
    private void listarMarcasComboBoxHilo(){
        Object ultimo = vista.dcbmMarcaVehiculo.getSelectedItem();
        listarMarcasComboBox();
        if (vehiculo != null) {
            vista.dcbmMarcaVehiculo.setSelectedItem(vehiculo.getMarca());
        }
        else{
            vista.dcbmMarcaVehiculo.setSelectedItem(ultimo);
        }
    }

    private void listarTalleres() {
        vista.dlmTaller.removeAllElements();
        for (Taller taller : modelo.getTalleres()) {
            vista.dlmTaller.addElement(taller);
        }
    }

    private void listarTalleresHilo() {
        listarTalleres();
        if (taller != null) {
            vista.listaTalleres.setSelectedValue(taller, true);
            mostrarTaller(taller);
        }
    }

    private void listarUsuarios() {
        vistaGestionUsuarios.dlmUsuarios.removeAllElements();
        for (Usuario usuario : modelo.getUsuarios()) {
            if (usuario.getRol() != 1) {
                vistaGestionUsuarios.dlmUsuarios.addElement(usuario);
            }
        }
    }

    private void listarUsuariosHilo() {
        listarUsuarios();
        if (usuario != null) {
            vistaGestionUsuarios.listaUsuarios.setSelectedValue(usuario, true);
            mostrarUsuario(usuario);
        }
    }

    private void listarMecanicos() {
        vista.dlmMecanico.removeAllElements();
        for (Mecanico mecanico : modelo.getMecanicos()) {
            vista.dlmMecanico.addElement(mecanico);
        }
    }

    private void listarMecanicosHilo() {
        listarMecanicos();
        if (mecanico != null) {
            vista.listaMecanicos.setSelectedValue(mecanico, true);
            mostrarMecanico(mecanico);
        }
    }

    private void listarReparaciones() {
        vista.dlmReparacion.removeAllElements();
        for (Reparacion reparacion : modelo.getReparaciones()) {
            vista.dlmReparacion.addElement(reparacion);
        }
    }

    private void listarReparacionesHilo() {
        listarReparaciones();
        if (reparacion != null) {
            vista.listaReparaciones.setSelectedValue(reparacion, true);
            mostrarReparacion(reparacion);
        }
    }

    private void listarVehiculos() {
        vista.dlmVehiculo.removeAllElements();
        for (Vehiculo vehiculo : modelo.getVehiculos()) {
            vista.dlmVehiculo.addElement(vehiculo);
        }
    }

    private void listarVehiculosHilo() {
        listarVehiculos();
        if (vehiculo != null) {
            vista.listaVehiculos.setSelectedValue(vehiculo, true);
            mostrarVehiculo(vehiculo);
        }
    }

    private void listarClientes() {
        vista.dlmCliente.removeAllElements();
        for (Cliente cliente : modelo.getClientes()) {
            vista.dlmCliente.addElement(cliente);
        }
    }

    private void listarClientesHilo() {
        listarClientes();
        if (cliente != null) {
            vista.listaClientes.setSelectedValue(cliente, true);
            mostrarCliente(cliente);
        }
    }


    private void cambiarIdiomaTextos(String idioma) throws IOException {
        this.selectorIdioma.load(new FileInputStream(idioma + ".props"));
        campoRequerido = selectorIdioma.getProperty("etCampoRequerido");
        vistaAcceso.etUsuarioLogin.setText(selectorIdioma.getProperty("etUsuarioLogin"));
        vistaAcceso.etPassLogin.setText(selectorIdioma.getProperty("etPassLogin"));
        vistaAcceso.btnAcceder.setText(selectorIdioma.getProperty("btnAcceder"));
        vistaAcceso.etSeleccionIdioma.setText(selectorIdioma.getProperty("etSeleccionIdioma"));
        vistaAcceso.txtContrasenya.setToolTipText(selectorIdioma.getProperty("txtPassLogin"));
        vistaAcceso.txtUsuario.setToolTipText(selectorIdioma.getProperty("txtUserLogin"));

        vistaGestionUsuarios.etUsuarioGestion.setText(selectorIdioma.getProperty("etUsuarioGestion"));
        vistaGestionUsuarios.etPassGestion.setText(selectorIdioma.getProperty("etPassGestion"));
        vistaGestionUsuarios.etRolGestion.setText(selectorIdioma.getProperty("etRolGestion"));
        vistaGestionUsuarios.btnModificar.setToolTipText(selectorIdioma.getProperty("btnModificar"));
        vistaGestionUsuarios.btnRegistrar.setToolTipText(selectorIdioma.getProperty("btnRegistrar"));
        vistaGestionUsuarios.btnEliminar.setToolTipText(selectorIdioma.getProperty("btnEliminar"));

        vista.menuContrasenya.setText(selectorIdioma.getProperty("menuPassword"));
        vista.menuAdmin.setText(selectorIdioma.getProperty("menuAdmin"));
        vista.menuSalir.setText(selectorIdioma.getProperty("menuSalir"));
        vista.itemContrasenya.setText(selectorIdioma.getProperty("itemContrasenya"));
        vista.itemAdmin.setText(selectorIdioma.getProperty("itemAdmin"));
        vista.itemSalir.setText(selectorIdioma.getProperty("itemSalir"));

        vista.tabbedPane1.setTitleAt(0, selectorIdioma.getProperty("panelTaller"));
        vista.tabbedPane1.setTitleAt(1, selectorIdioma.getProperty("panelMecanico"));
        vista.tabbedPane1.setTitleAt(2, selectorIdioma.getProperty("panelReparacion"));
        vista.tabbedPane1.setTitleAt(3, selectorIdioma.getProperty("panelCliente"));
        vista.tabbedPane1.setTitleAt(4, selectorIdioma.getProperty("panelVehiculo"));
        vista.tabbedPane1.setTitleAt(5, selectorIdioma.getProperty("panelInformes"));

        vista.btnInformeReparacionesAnyo.setText(selectorIdioma.getProperty("btnInformeReparacionesAnyo"));
        vista.etInformeReparacionesAnyo.setText(selectorIdioma.getProperty("etInformeReparacionesAnyo"));
        vista.btnInformeGrafico.setText(selectorIdioma.getProperty("btnInformeGrafico"));
        vista.etInformeGrafico.setText(selectorIdioma.getProperty("etInformeGrafico"));
        vista.btnInformeClientes.setText(selectorIdioma.getProperty("btnInformeClientes"));
        vista.etInformeClientes.setText(selectorIdioma.getProperty("etInformeClientes"));


        vista.etTelefonoTaller.setText(selectorIdioma.getProperty("etTelefonoTaller"));
        vista.etNombreTaller.setText(selectorIdioma.getProperty("etNombreTaller"));
        vista.etIdTaller.setText(selectorIdioma.getProperty("etIdTaller"));
        vista.etTiendaOnlineTaller.setText(selectorIdioma.getProperty("etTiendaOnlineTaller"));
        vista.etHorarioCierreTaller.setText(selectorIdioma.getProperty("etHorarioCierreTaller"));
        vista.etUbicacionTaller.setText(selectorIdioma.getProperty("etUbicacionTaller"));
        vista.etFechaAperturaTaller.setText(selectorIdioma.getProperty("etFechaAperturaTaller"));
        vista.etHorarioAperturaTaller.setText(selectorIdioma.getProperty("etHorarioAperturaTaller"));
        vista.btnAltaTaller.setToolTipText(selectorIdioma.getProperty("btnAltaTaller"));
        vista.btnModificarTaller.setToolTipText(selectorIdioma.getProperty("btnModificarTaller"));
        vista.btnEliminarTaller.setToolTipText(selectorIdioma.getProperty("btnEliminarTaller"));


        vista.etEspecialidadMecanico.setText(selectorIdioma.getProperty("etEspecialidadMecanico"));
        vista.etTallerMecanico.setText(selectorIdioma.getProperty("etTallerMecanico"));
        vista.etExperienciaMecanico.setText(selectorIdioma.getProperty("etExperienciaMecanico"));
        vista.etIncorporacionMecanico.setText(selectorIdioma.getProperty("etIncorporacionMecanico"));
        vista.etNEmpleadoMecanico.setText(selectorIdioma.getProperty("etNEmpleadoMecanico"));
        vista.etDniMecanico.setText(selectorIdioma.getProperty("etDniMecanico"));
        vista.etApellidosMecanico.setText(selectorIdioma.getProperty("etApellidosMecanico"));
        vista.etNombreMecanico.setText(selectorIdioma.getProperty("etNombreMecanico"));
        vista.etIdMecanico.setText(selectorIdioma.getProperty("etIdMecanico"));
        vista.btnRegistrarMecanico.setToolTipText(selectorIdioma.getProperty("btnRegistrarMecanico"));
        vista.btnModificarMecanico.setToolTipText(selectorIdioma.getProperty("btnModificarMecanico"));
        vista.btnEliminarMecanico.setToolTipText(selectorIdioma.getProperty("btnEliminarMecanico"));


        vista.etDiagnosticoReparacion.setText(selectorIdioma.getProperty("etDiagnosticoReparacion"));
        vista.etMatriculaReparacion.setText(selectorIdioma.getProperty("etMatriculaReparacion"));
        vista.btnEliminarReparacion.setToolTipText(selectorIdioma.getProperty("btnEliminarReparacion"));
        vista.btnRegistrarReparacion.setToolTipText(selectorIdioma.getProperty("btnRegistrarReparacion"));
        vista.btnModificarReparacion.setToolTipText(selectorIdioma.getProperty("btnModificarReparacion"));
        vista.etCosteReparacion.setText(selectorIdioma.getProperty("etCosteReparacion"));
        vista.etEmisionReparacion.setText(selectorIdioma.getProperty("etEmisionReparacion"));
        vista.etEntregaReparacion.setText(selectorIdioma.getProperty("etEntregaReparacion"));
        vista.etIdReparacion.setText(selectorIdioma.getProperty("etIdReparacion"));

        vista.etIdCliente.setText(selectorIdioma.getProperty("etIdCliente"));
        vista.etNombreCliente.setText(selectorIdioma.getProperty("etNombreCliente"));
        vista.etApellidosCliente.setText(selectorIdioma.getProperty("etApellidosCliente"));
        vista.etDniCliente.setText(selectorIdioma.getProperty("etDniCliente"));
        vista.etTelefonoCliente.setText(selectorIdioma.getProperty("etTelefonoCliente"));
        vista.etFechaNacimientoCliente.setText(selectorIdioma.getProperty("etFechaNacimientoCliente"));
        vista.etDomicilioCliente.setText(selectorIdioma.getProperty("etDomicilioCliente"));
        vista.etCpCliente.setText(selectorIdioma.getProperty("etCpCliente"));
        vista.chSocioCliente.setText(selectorIdioma.getProperty("chSocioCliente"));
        vista.btnRegistrarCliente.setToolTipText(selectorIdioma.getProperty("btnRegistrarCliente"));
        vista.btnModificarCliente.setToolTipText(selectorIdioma.getProperty("btnModificarCliente"));
        vista.btnEliminarCliente.setToolTipText(selectorIdioma.getProperty("btnEliminarCliente"));

        vista.etIdVehiculo.setText(selectorIdioma.getProperty("etIdVehiculo"));
        vista.etImagendeVehiculo.setText(selectorIdioma.getProperty("etImagendeVehiculo"));
        vista.btnSeleccionarImagen.setText(selectorIdioma.getProperty("btnSeleccionarImagen"));
        vista.etNChasisVehiculo.setText(selectorIdioma.getProperty("etNChasisVehiculo"));
        vista.etMatriculacionVehiculo.setText(selectorIdioma.getProperty("etMatriculacionVehiculo"));
        vista.etMarcaVehiculo.setText(selectorIdioma.getProperty("etMarcaVehiculo"));
        vista.etModeloVehiculo.setText(selectorIdioma.getProperty("etModeloVehiculo"));
        vista.etMatriculaVehiculo.setText(selectorIdioma.getProperty("etMatriculaVehiculo"));
        vista.etCombustibleVehiculo.setText(selectorIdioma.getProperty("etCombustibleVehiculo"));
        vista.etPropietarioVehiculo.setText(selectorIdioma.getProperty("etPropietarioVehiculo"));
        vista.btnRegistrarVehiculo.setToolTipText(selectorIdioma.getProperty("btnRegistrarVehiculo"));
        vista.btnModificarVehiculo.setToolTipText(selectorIdioma.getProperty("btnModificarVehiculo"));
        vista.btnEliminarVehiculo.setToolTipText(selectorIdioma.getProperty("btnEliminarVehiculo"));
    }

    private class Actualizar extends Thread {

        @Override
        public void run() {

            while (true) {
                try {
                    sleep(500000);
                    listarClientesComboBoxHilo();
                    listarCombustiblesComoBoxHilo();
                    listarMarcasComboBoxHilo();
                    listarMatriculasComboBoxHilo();
                    listarTalleresComboBoxHilo();
                    listarClientesHilo();
                    listarMecanicosHilo();
                    listarVehiculosHilo();
                    listarTalleresHilo();
                    listarUsuariosHilo();
                    listarReparacionesHilo();


                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public VistaAcceso getVistaAcceso() {
        return vistaAcceso;
    }

    public Vista getVista() {
        return vista;
    }

    public Modelo getModelo() {
        return modelo;
    }

    public VistaGestionUsuarios getVistaGestionUsuarios() {
        return vistaGestionUsuarios;
    }
}
