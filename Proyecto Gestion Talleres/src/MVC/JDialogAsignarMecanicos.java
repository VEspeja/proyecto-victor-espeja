package MVC;

import base.Mecanico;
import base.Reparacion;

import javax.swing.*;
import java.awt.event.*;
import java.util.Properties;

public class JDialogAsignarMecanicos extends JDialog {
    private JPanel contentPane;
    private JButton btnAsignar;
    private JButton btnDesvincular;
    private JList listaMecanicosDisponibles;
    private JList listaMecanicosAsignados;
    private JLabel etMecanicosDisponibles;
    private JLabel etMecanicosAsignados;
    Reparacion reparacion;
    Modelo modelo;
    DefaultListModel<Mecanico> dlmMecanicosAsignados;
    DefaultListModel<Mecanico> dlmMecanicosDisponibles;
    Properties selectorIdioma;

    public JDialogAsignarMecanicos(Reparacion reparacion, Modelo modelo, Properties selector) {
        this.reparacion = reparacion;
        this.modelo = modelo;
        setContentPane(contentPane);
        pack();
        setVisible(true);
        setModal(true);
        ImageIcon logo = new ImageIcon("imagenes/iconos/logo_aplicacion.png");
        setIconImage(logo.getImage());

        //getRootPane().setDefaultButton(buttonOk);
        this.setLocationRelativeTo(null);
        selectorIdioma = selector;
        setTitle(selectorIdioma.getProperty("panelAsignar"));
        etMecanicosAsignados.setText(selectorIdioma.getProperty("etMecanicosAsignados"));
        etMecanicosDisponibles.setText(selectorIdioma.getProperty("etMecanicosDisponibles"));
        btnAsignar.setIcon(new ImageIcon("imagenes/iconos/add.png"));
        btnAsignar.setToolTipText(selectorIdioma.getProperty("btnAsignar"));
        btnDesvincular.setIcon(new ImageIcon("imagenes/iconos/delete.png"));
        btnDesvincular.setToolTipText(selectorIdioma.getProperty("btnDesvincular"));

        dlmMecanicosAsignados = new DefaultListModel<>();
        listaMecanicosAsignados.setModel(dlmMecanicosAsignados);

        dlmMecanicosDisponibles = new DefaultListModel<>();
        listaMecanicosDisponibles.setModel(dlmMecanicosDisponibles);

        mostrarMecanicos();

        btnAsignar.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                asignar();
            }
        });

        btnDesvincular.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                desvincular();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

    }

    private void mostrarMecanicos() {
        System.out.println(this.reparacion);
        dlmMecanicosAsignados.removeAllElements();
        dlmMecanicosDisponibles.removeAllElements();
        if(reparacion.getListaMecanicos().size()==0) {
            for (Mecanico mecanico : modelo.obtenerMecanicosDisponiblesParaRepararReparacionLibre(this.reparacion)) {
                dlmMecanicosDisponibles.addElement(mecanico);
            }
        }
        else{
            for (Mecanico mecanico : modelo.obtenerMecanicosDisponiblesParaRepararReparacionAsignada(this.reparacion)) {
                dlmMecanicosDisponibles.addElement(mecanico);
            }
        }

        for (Mecanico mecanico : this.reparacion.getListaMecanicos()){
            dlmMecanicosAsignados.addElement(mecanico);
        }
    }

    private void onCancel() {
        dispose();
    }

    private void asignar() {
        this.reparacion.asignarMecanicos(listaMecanicosDisponibles.getSelectedValuesList());
        this.modelo.modificarReparacion(this.reparacion);
        mostrarMecanicos();
    }

    private void desvincular() {
        this.reparacion.desvincularEntrenadores(listaMecanicosAsignados.getSelectedValuesList());
        this.modelo.modificarReparacion(this.reparacion);
        mostrarMecanicos();
    }

}
