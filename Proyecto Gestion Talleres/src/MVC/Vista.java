package MVC;

import base.*;
import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;
import java.awt.*;

public class Vista {
    public JFrame frame;
    public JTabbedPane tabbedPane1;
    private JPanel panel1;
    public JCheckBox chTiendaOnlineTaller;
    public JTextField txtHorarioCierreTaller;
    public JTextField txtIdTaller;
    public JTextField txtNombreTaller;
    public JTextField txtUbicacionTaller;
    public JTextField txtTelefonoTaller;
    public JTextField txtHorarioAperturaTaller;
    public JList listaTalleres;
    public JButton btnAltaTaller;
    public JButton btnModificarTaller;
    public JButton btnEliminarTaller;
    public JTextField txtEspecialidadMecanico;
    public JComboBox cbTallerMecanico;
    public JTextField txtExperienciaMecanico;
    public DatePicker dtIncorporacionMecanico;
    public JTextField txtNEmpleadoMecanico;
    public JTextField txtDniMecanico;
    public JTextField txtApellidosMecanico;
    public JTextField txtNombreMecanico;
    public JTextField txtIdMecanico;
    public JTextArea taDiagnosticoReparacion;
    public JComboBox cbMatriculaReparacion;
    public JButton btnEliminarReparacion;
    public JButton btnModificarReparacion;
    public JButton btnRegistrarReparacion;
    public JTextField txtCosteReparacion;
    public JTextField txtIdReparacion;
    public JButton btnRegistrarMecanico;
    public JButton btnModificarMecanico;
    public JButton btnEliminarMecanico;
    public JList listaMecanicos;
    public JList listaReparaciones;
    public JTextField txtIdCliente;
    public JTextField txtNombreCliente;
    public JTextField txtApellidosCliente;
    public JTextField txtDniCliente;
    public JTextField txtTelefonoCliente;
    public DatePicker dtFechaNacimientoCliente;
    public JTextField txtDomicilioCliente;
    public JTextField txtCpCliente;
    public JCheckBox chSocioCliente;
    public JButton btnEliminarCliente;
    public JButton btnModificarCliente;
    public JButton btnRegistrarCliente;
    public JList listaClientes;
    public JButton btnSeleccionarImagen;
    public JTextField txtNChasisVehiculo;
    public DatePicker dtMatriculacionVehiculo;
    public JComboBox cbMarcaVehiculo;
    public JTextField txtModeloVehiculo;
    public JComboBox cbCombustibleVehiculo;
    public JTextField txtIdVehiculo;
    public JComboBox cbPropietarioVehiculo;
    public JButton btnRegistrarVehiculo;
    public JButton btnModificarVehiculo;
    public JButton btnEliminarVehiculo;
    public JList listaVehiculos;
    public DatePicker dtFechaAperturaTaller;
    public DatePicker dtEmisionReparacion;
    public DatePicker dtEntregaReparacion;
    public JTextField txtMatriculaVehiculo;
    public JLabel etImagenVehiculo;
    public JCheckBox chEspecialidadMecanico;
    public JLabel etTiendaOnlineTaller;
    public JLabel etHorarioCierreTaller;
    public JLabel etIdTaller;
    public JLabel etNombreTaller;
    public JLabel etUbicacionTaller;
    public JLabel etTelefonoTaller;
    public JLabel etFechaAperturaTaller;
    public JLabel etHorarioAperturaTaller;
    public JLabel etIdMecanico;
    public JLabel etNombreMecanico;
    public JLabel etApellidosMecanico;
    public JLabel etNEmpleadoMecanico;
    public JLabel etIncorporacionMecanico;
    public JLabel etExperienciaMecanico;
    public JLabel etTallerMecanico;
    public JLabel etEspecialidadMecanico;
    public JLabel etDniMecanico;
    public JLabel etDiagnosticoReparacion;
    public JLabel etMatriculaReparacion;
    public JLabel etEntregaReparacion;
    public JLabel etEmisionReparacion;
    public JLabel etIdReparacion;
    public JLabel etCosteReparacion;
    public JLabel etIdCliente;
    public JLabel etNombreCliente;
    public JLabel etApellidosCliente;
    public JLabel etDniCliente;
    public JLabel etTelefonoCliente;
    public JLabel etFechaNacimientoCliente;
    public JLabel etDomicilioCliente;
    public JLabel etCpCliente;
    public JLabel etNChasisVehiculo;
    public JLabel etMatriculacionVehiculo;
    public JLabel etMarcaVehiculo;
    public JLabel etModeloVehiculo;
    public JLabel etCombustibleVehiculo;
    public JLabel etIdVehiculo;
    public JLabel etPropietarioVehiculo;
    public JLabel etMatriculaVehiculo;
    public JLabel etImagendeVehiculo;
    public JButton btnAsignarMecanicos;
    public JPanel panelMecanico;
    public JPanel panelTaller;
    public JPanel panelReparacion;
    public JPanel panelCliente;
    public JPanel panelVehiculo;
    public JButton btnBuscarTalleres;
    public JButton btnBuscarMecanicos;
    public JButton btnBuscarReparaciones;
    public JButton btnBuscarClientes;
    public JButton btnBuscarVehiculos;
    public JButton btnInformeReparacionesAnyo;
    public JComboBox cbAnyoInforme;
    public JButton btnInformeGrafico;
    public JLabel etMarcaVehiculoValor;
    public JLabel etModeloVehiculoValor;
    public JLabel etMatriculaVehiculoValor;
    public JLabel etCombustibleVehiculoValor;
    public JLabel etFechaMatriculacionVehiculoValor;
    public JLabel etNChasisVehiculoValor;
    public JLabel etPropietarioVehiculoValor;
    public JLabel etNombreClienteValor;
    public JLabel etApellidosClienteValor;
    public JLabel etDniClienteValor;
    public JLabel etTelefonoClienteValor;
    public JLabel etDomicilioClienteValor;
    public JLabel etFechaNacimientoClienteValor;
    public JLabel etNombreMecanicoValor;
    public JLabel etApellidosMecanicoValor;
    public JLabel etDniMecanicoValor;
    public JLabel etNEmpleadoMecanicoValor;
    public JLabel etFechaIncorporacionMecanicoValor;
    public JLabel etExperienciaMecanicoValor;
    public JLabel etTallerMecanicoValor;
    public JPanel panelInformes;
    public JLabel etInformeReparacionesAnyo;
    public JLabel etInformeGrafico;
    public JButton btnInformeClientes;
    public JLabel etInformeClientes;

    public JMenuBar barra;
    public JMenu menuSalir;
    public JMenu menuAdmin;
    public JMenu menuContrasenya;
    public JMenuItem itemContrasenya;
    public JMenuItem itemSalir;
    public JMenuItem itemAdmin;

    public DefaultListModel<Vehiculo> dlmVehiculo;
    public DefaultListModel<Taller> dlmTaller;
    public DefaultListModel<Cliente> dlmCliente;
    public DefaultListModel<Mecanico> dlmMecanico;
    public DefaultListModel<Reparacion> dlmReparacion;

    public DefaultComboBoxModel<String> dcbmMarcaVehiculo;
    public DefaultComboBoxModel<String> dcbmCombustibleVehiculo;
    public DefaultComboBoxModel<Cliente> dcbmClienteVehiculo;
    public DefaultComboBoxModel<Taller> dcbmTallerMecanico;
    public DefaultComboBoxModel<String> dcbmMatriculaReparacion;
    public DefaultComboBoxModel<String> dcbmAnyoInforme;

    public Vista() {
        this.frame = new JFrame("Gestor de Talleres v.1.0");
        this.frame.setContentPane(panel1);
        this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.frame.pack();
        this.frame.setVisible(false);
        this.frame.setLocationRelativeTo(null);
        ImageIcon logo = new ImageIcon("imagenes/iconos/logo_aplicacion.png");
        this.frame.setIconImage(logo.getImage());
        init();
    }

    private void init(){
        dlmTaller = new DefaultListModel<Taller>();
        listaTalleres.setModel(dlmTaller);

        dlmMecanico = new DefaultListModel<Mecanico>();
        listaMecanicos.setModel(dlmMecanico);

        dlmCliente = new DefaultListModel<Cliente>();
        listaClientes.setModel(dlmCliente);

        dlmVehiculo = new DefaultListModel<Vehiculo>();
        listaVehiculos.setModel(dlmVehiculo);

        dlmReparacion = new DefaultListModel<Reparacion>();
        listaReparaciones.setModel(dlmReparacion);

        dcbmMarcaVehiculo = new DefaultComboBoxModel<>();
        cbMarcaVehiculo.setModel(dcbmMarcaVehiculo);

        dcbmCombustibleVehiculo = new DefaultComboBoxModel<>();
        cbCombustibleVehiculo.setModel(dcbmCombustibleVehiculo);

        dcbmTallerMecanico = new DefaultComboBoxModel<Taller>();
        cbTallerMecanico.setModel(dcbmTallerMecanico);

        dcbmClienteVehiculo = new DefaultComboBoxModel<Cliente>();
        cbPropietarioVehiculo.setModel(dcbmClienteVehiculo);

        dcbmMatriculaReparacion = new DefaultComboBoxModel();
        cbMatriculaReparacion.setModel(dcbmMatriculaReparacion);

        dcbmAnyoInforme = new DefaultComboBoxModel<>();
        cbAnyoInforme.setModel(dcbmAnyoInforme);

        ImageIcon datePicker = new ImageIcon("imagenes/iconos/datepicker.png");

        dtMatriculacionVehiculo.getComponentToggleCalendarButton().setText("");
        dtMatriculacionVehiculo.getComponentToggleCalendarButton().setIcon(datePicker);
        dtEmisionReparacion.getComponentToggleCalendarButton().setText("");
        dtEmisionReparacion.getComponentToggleCalendarButton().setIcon(datePicker);
        dtEntregaReparacion.getComponentToggleCalendarButton().setText("");
        dtEntregaReparacion.getComponentToggleCalendarButton().setIcon(datePicker);
        dtFechaAperturaTaller.getComponentToggleCalendarButton().setText("");
        dtFechaAperturaTaller.getComponentToggleCalendarButton().setIcon(datePicker);
        dtFechaNacimientoCliente.getComponentToggleCalendarButton().setText("");
        dtFechaNacimientoCliente.getComponentToggleCalendarButton().setIcon(datePicker);
        dtIncorporacionMecanico.getComponentToggleCalendarButton().setText("");
        dtIncorporacionMecanico.getComponentToggleCalendarButton().setIcon(datePicker);

        btnAsignarMecanicos.setIcon(new ImageIcon("imagenes/iconos/add-user.png"));
        btnBuscarTalleres.setIcon(new ImageIcon("imagenes/iconos/search.png"));
        btnBuscarVehiculos.setIcon(new ImageIcon("imagenes/iconos/search.png"));
        btnBuscarMecanicos.setIcon(new ImageIcon("imagenes/iconos/search.png"));
        btnBuscarClientes.setIcon(new ImageIcon("imagenes/iconos/search.png"));
        btnBuscarReparaciones.setIcon(new ImageIcon("imagenes/iconos/search.png"));

        btnEliminarCliente.setIcon(new ImageIcon("imagenes/iconos/delete.png"));
        btnEliminarTaller.setIcon(new ImageIcon("imagenes/iconos/delete.png"));
        btnEliminarMecanico.setIcon(new ImageIcon("imagenes/iconos/delete.png"));
        btnEliminarVehiculo.setIcon(new ImageIcon("imagenes/iconos/delete.png"));
        btnEliminarReparacion.setIcon(new ImageIcon("imagenes/iconos/delete.png"));

        btnModificarCliente.setIcon(new ImageIcon("imagenes/iconos/update.png"));
        btnModificarVehiculo.setIcon(new ImageIcon("imagenes/iconos/update.png"));
        btnModificarTaller.setIcon(new ImageIcon("imagenes/iconos/update.png"));
        btnModificarMecanico.setIcon(new ImageIcon("imagenes/iconos/update.png"));
        btnModificarReparacion.setIcon(new ImageIcon("imagenes/iconos/update.png"));

        btnRegistrarCliente.setIcon(new ImageIcon("imagenes/iconos/add.png"));
        btnRegistrarVehiculo.setIcon(new ImageIcon("imagenes/iconos/add.png"));
        btnRegistrarReparacion.setIcon(new ImageIcon("imagenes/iconos/add.png"));
        btnRegistrarMecanico.setIcon(new ImageIcon("imagenes/iconos/add.png"));
        btnAltaTaller.setIcon(new ImageIcon("imagenes/iconos/add.png"));

        crearMenu();
    }

    private void crearMenu() {
        barra = new JMenuBar();
        menuContrasenya = new JMenu("Contraseña");
        menuAdmin = new JMenu("Administracion");
        menuSalir = new JMenu("Desconexion");

        itemContrasenya = new JMenuItem("Cambiar Contraseña");
        itemContrasenya.setIcon(new ImageIcon("imagenes/iconos/update-user.png"));
        itemContrasenya.setActionCommand("Cambiar Pass");

        itemAdmin = new JMenuItem("Gestionar Usuarios");
        itemAdmin.setIcon(new ImageIcon("imagenes/iconos/gestion.png"));
        itemAdmin.setActionCommand("Gestionar Usuarios");

        itemSalir = new JMenuItem("Desconectar");
        itemSalir.setIcon(new ImageIcon("imagenes/iconos/exit.png"));
        itemSalir.setActionCommand("Regresar");

        menuAdmin.add(itemAdmin);
        menuSalir.add(itemSalir);
        menuContrasenya.add(itemContrasenya);

        barra.add(menuSalir);
        barra.add(menuAdmin);
        barra.add(menuContrasenya);

        frame.setJMenuBar(barra);
    }


}
