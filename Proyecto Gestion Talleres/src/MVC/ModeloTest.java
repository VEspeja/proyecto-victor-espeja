package MVC;

import base.*;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import principal.HibernateUtil;

import java.time.LocalDate;
import java.util.List;

public class ModeloTest extends Modelo{
    private static Usuario user;
    private static Mecanico mecanico;
    private Taller taller = new Taller("prueba", "aa",123456789, LocalDate.now(),"8","8",false);

    private static Cliente cliente;
    private static Reparacion reparacion;
    private static Vehiculo vehiculo;

    @BeforeClass
    public static void setUpClass(){
        user = new Usuario("prueba","1234",2);
        mecanico = new Mecanico("prueba","prueba","111111111X","0", LocalDate.now(),1,null,null);
        cliente = new Cliente("prueba","prueba","prueba",111111111,LocalDate.now(),"prueba","prueba",false);
        vehiculo = new Vehiculo(56,"a1","audi","1234ZZZ","gasolina", LocalDate.now(),"1111","imagenes/vehiculos/audi.png",cliente);
        reparacion = new Reparacion(LocalDate.now(),10,LocalDate.now(),"aaa",vehiculo);

    }

    @Test
    public void conectar1() {
        this.conectar();
        Assert.assertEquals(true, HibernateUtil.getSession().isOpen());
    }
    @Test
    public void altaTaller() {

        List lista = this.getTalleres();
        int contador = lista.size();
        this.altaTaller(taller);

        lista = this.getTalleres();
        Assert.assertTrue("Se ha insertado correctamente", contador + 1 == lista.size());
        eliminarTaller(taller);
    }

    @Test
    public void altaVehiculo() {
        altaCliente(cliente);
        List lista = this.getVehiculos();
        int cantidad = lista.size();
        this.altaVehiculoTest(vehiculo);

        lista = this.getVehiculos();
        Assert.assertTrue("Se ha insertado correctamente", cantidad + 1 == lista.size());
        eliminarVehiculo(vehiculo);
        eliminarCliente(cliente);
    }
    @Test
    public void altaClienteEquals(){
        List lista = this.getClientes();
        int cantidad = lista.size();
        altaCliente(cliente);
        lista = this.getClientes();
        Assert.assertEquals(cantidad+1,lista.size());
        eliminarCliente(cliente);
    }
    @Test
    public void eliminarClienteNegativo(){
        eliminarCliente(cliente);
        List lista = getClientes();
        Assert.assertFalse(lista.contains(cliente));
    }

    @Test
    public void eliminarMecanicoAfirmativo() {
        altaMecanico(mecanico);
        int cantidad = getMecanicos().size();
        eliminarMecanico(mecanico);
        Assert.assertTrue("El mecanico se ha eliminado correctamente",getMecanicos().size() == (cantidad-1));
    }

    @Test
    public void eliminarMecanicoIgual() {
        int cantidadAntes = getMecanicos().size();
        altaMecanico(mecanico);
        eliminarMecanico(mecanico);
        int cantidadDespues = getMecanicos().size();
        Assert.assertEquals(cantidadAntes,cantidadDespues);
    }

    @Test
    public void registrarReparacion() {
    altaClienteTest(cliente);
        altaVehiculoTest(vehiculo);
        List lista = getReparaciones();
        int cantidad = lista.size();
        altaReparacionTest(reparacion);
        Assert.assertEquals(cantidad+1,getReparaciones().size());
        eliminarReparacionTest(reparacion);
        eliminarVehiculo(vehiculo);
        eliminarCliente(cliente);

    }
    @Test
    public void modificarTaller() {
        altaTaller(taller);
        taller.setUbicacion("aqui");
        modificarTallerTest(taller);
        Assert.assertEquals("prueba",buscarTallerUbicacion("aqui").getNombre());
        eliminarTallerTest(taller);
    }

    @Test
    public void BuscarTallerPorUbicacion() {
        altaTaller(taller);
        Assert.assertEquals("prueba",buscarTallerUbicacion("aa").getNombre());
        eliminarTallerTest(taller);
    }

   @Test
    public void BuscarUser() {
        altaUsuarioTest(user);
        Assert.assertEquals("prueba",buscarUsuarioNombre("prueba").getNameUser());
        eliminarUsuario(user);
    }

}