package MVC;

import base.Mecanico;
import base.Taller;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.List;
import java.util.Objects;
import java.util.Properties;

public class JDialogBusqueda extends JDialog implements KeyListener{
    private JPanel contentPane;
    private JButton btnBuscar;
    private JLabel etFiltroBusqueda;
    private JComboBox cbFiltro;
    private JTextField txtBusqueda;
    private JList listaBusqueda;
    private JCheckBox chBusqueda;
    private String tabla;
    private Modelo modelo;
    private DefaultListModel dlmListaBusqueda;
    private DefaultComboBoxModel dcbmBusqueda;
    Properties selectorIdioma;

    public JDialogBusqueda(String tabla, Modelo modelo, Properties selector) {
        this.tabla = tabla;
        this.modelo = modelo;
        selectorIdioma = selector;
        setContentPane(contentPane);
        setLocationRelativeTo(null);
        pack();
        setVisible(true);
        setModal(true);
        getRootPane().setDefaultButton(btnBuscar);
        ImageIcon logo = new ImageIcon("imagenes/iconos/logo_aplicacion.png");
        setIconImage(logo.getImage());

        init();

        txtBusqueda.addKeyListener(this);

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

    }

    private void init() {
        dcbmBusqueda = new DefaultComboBoxModel();
        cbFiltro.setModel(dcbmBusqueda);
        etFiltroBusqueda.setText(selectorIdioma.getProperty("etFiltroBusqueda"));

        if(tabla.equals("Taller")){
            setTitle(selectorIdioma.getProperty("ventanaBusquedaTalleres"));
            dcbmBusqueda.addElement(selectorIdioma.getProperty("etBusquedaNombre"));
            dcbmBusqueda.addElement(selectorIdioma.getProperty("etBusquedaUbicacion"));
            dcbmBusqueda.addElement(selectorIdioma.getProperty("etBusquedaTelefono"));
            dcbmBusqueda.addElement(selectorIdioma.getProperty("etBusquedaFechaApertura"));
            dcbmBusqueda.addElement(selectorIdioma.getProperty("etBusquedaHorarioApertura"));
            dcbmBusqueda.addElement(selectorIdioma.getProperty("etBusquedaHorarioCierre"));
            chBusqueda.setText(selectorIdioma.getProperty("chBusquedaTaller"));
        }
        if(tabla.equals("Mecanico")){
            setTitle(selectorIdioma.getProperty("ventanaBusquedaMecanicos"));
            dcbmBusqueda.addElement(selectorIdioma.getProperty("etBusquedaNombre"));
            dcbmBusqueda.addElement(selectorIdioma.getProperty("etBusquedaApellidos"));
            dcbmBusqueda.addElement(selectorIdioma.getProperty("etBusquedaDNI"));
            dcbmBusqueda.addElement(selectorIdioma.getProperty("etBusquedaNumeroEmpleado"));
            dcbmBusqueda.addElement(selectorIdioma.getProperty("etBusquedaFechaIncorporacion"));
            dcbmBusqueda.addElement(selectorIdioma.getProperty("etBusquedaExperiencia"));
            dcbmBusqueda.addElement(selectorIdioma.getProperty("etBusquedaEspecialidad"));
            dcbmBusqueda.addElement(selectorIdioma.getProperty("etBusquedaNombreTaller"));
            chBusqueda.setVisible(false);
        }
        if(tabla.equals("Reparacion")){
            setTitle(selectorIdioma.getProperty("ventanaBusquedaReparaciones"));
            dcbmBusqueda.addElement(selectorIdioma.getProperty("etBusquedaFechaEmision"));
            dcbmBusqueda.addElement(selectorIdioma.getProperty("etBusquedaFechaEntrega"));
            dcbmBusqueda.addElement(selectorIdioma.getProperty("etBusquedaDiagnostico"));
            dcbmBusqueda.addElement(selectorIdioma.getProperty("etBusquedaCoste"));
            dcbmBusqueda.addElement(selectorIdioma.getProperty("etBusquedaMatriculaVehiculo"));
            chBusqueda.setVisible(false);
        }
        if(tabla.equals("Cliente")){
            setTitle(selectorIdioma.getProperty("ventanaBusquedaClientes"));
            dcbmBusqueda.addElement(selectorIdioma.getProperty("etBusquedaNombre"));
            dcbmBusqueda.addElement(selectorIdioma.getProperty("etBusquedaApellidos"));
            dcbmBusqueda.addElement(selectorIdioma.getProperty("etBusquedaDNI"));
            dcbmBusqueda.addElement(selectorIdioma.getProperty("etBusquedaTelefonoContacto"));
            dcbmBusqueda.addElement(selectorIdioma.getProperty("etBusquedaFechaNacimiento"));
            dcbmBusqueda.addElement(selectorIdioma.getProperty("etBusquedaDireccion"));
            dcbmBusqueda.addElement(selectorIdioma.getProperty("etBusquedaCodigoPostal"));
            chBusqueda.setText(selectorIdioma.getProperty("chBusquedaCliente"));
        }
        if(tabla.equals("Vehiculo")){
            setTitle(selectorIdioma.getProperty("ventanaBusquedaVehiculos"));
            dcbmBusqueda.addElement(selectorIdioma.getProperty("etBusquedaMarca"));
            dcbmBusqueda.addElement(selectorIdioma.getProperty("etBusquedaModelo"));
            dcbmBusqueda.addElement(selectorIdioma.getProperty("etBusquedaMatricula"));
            dcbmBusqueda.addElement(selectorIdioma.getProperty("etBusquedaCombustible"));
            dcbmBusqueda.addElement(selectorIdioma.getProperty("etBusquedaFechaMatriculacion"));
            dcbmBusqueda.addElement(selectorIdioma.getProperty("etBusquedaNumeroChasis"));
            dcbmBusqueda.addElement(selectorIdioma.getProperty("etBusquedaPropietario"));
            chBusqueda.setVisible(false);
        }

        dlmListaBusqueda = new DefaultListModel();
        listaBusqueda.setModel(dlmListaBusqueda);
    }


    private void onCancel() {
        // add your code here if necessary
        dispose();
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

    }


    @Override
    public void keyReleased(KeyEvent e) {
        if (e.getSource() == txtBusqueda) {
            if (txtBusqueda.getText().length() > 0) {
                dlmListaBusqueda.removeAllElements();
                List resultado = null;
                if(tabla.equals("Taller")){
                    if(chBusqueda.isSelected()){
                        if (dcbmBusqueda.getSelectedItem().toString().equals("Fecha Apertura")) {
                            resultado = modelo.mostrarBusqueda2Criterios(tabla, "fecha_apertura","tiendaOnline", "'" + txtBusqueda.getText() + "%'");
                        } else if (dcbmBusqueda.getSelectedItem().toString().equals("Horario Apertura")) {
                            resultado = modelo.mostrarBusqueda2Criterios(tabla, "horario_apertura","tiendaOnline", "'" + txtBusqueda.getText() + "%'");
                        } else if (dcbmBusqueda.getSelectedItem().toString().equals("Horario Cierre")) {
                            resultado = modelo.mostrarBusqueda2Criterios(tabla, "horario_cierre","tiendaOnline", "'" + txtBusqueda.getText() + "%'");
                        } else {
                            resultado = modelo.mostrarBusqueda2Criterios(tabla, dcbmBusqueda.getSelectedItem().toString().toLowerCase(),"tiendaOnline", "'" + txtBusqueda.getText() + "%'");
                        }
                    }
                    else {
                        if (dcbmBusqueda.getSelectedItem().toString().equals("Fecha Apertura")) {
                            resultado = modelo.mostrarBusqueda(tabla, "fecha_apertura", "'" + txtBusqueda.getText() + "%'");
                        } else if (dcbmBusqueda.getSelectedItem().toString().equals("Horario Apertura")) {
                            resultado = modelo.mostrarBusqueda(tabla, "horario_apertura", "'" + txtBusqueda.getText() + "%'");
                        } else if (dcbmBusqueda.getSelectedItem().toString().equals("Horario Cierre")) {
                            resultado = modelo.mostrarBusqueda(tabla, "horario_cierre", "'" + txtBusqueda.getText() + "%'");
                        } else if (dcbmBusqueda.getSelectedItem().toString().equals("Tienda Online")) {
                            resultado = modelo.mostrarBusqueda(tabla, "tienda_online", "'" + txtBusqueda.getText() + "%'");
                        } else {
                            resultado = modelo.mostrarBusqueda(tabla, dcbmBusqueda.getSelectedItem().toString().toLowerCase(), "'" + txtBusqueda.getText() + "%'");
                        }
                    }

                }
                if(tabla.equals("Mecanico")){
                    if(dcbmBusqueda.getSelectedItem().toString().equals("Nombre Taller")){
                        resultado = modelo.mostrarBusquedaRelacion(tabla,"Taller","taller","nombre","'"+txtBusqueda.getText()+"%'");
                    }
                    else if(dcbmBusqueda.getSelectedItem().toString().equals("Numero de Empleado")){
                        resultado = modelo.mostrarBusqueda(tabla,"numero_empleado","'"+txtBusqueda.getText()+"%'");
                    }
                    else if(dcbmBusqueda.getSelectedItem().toString().equals("Fecha de Incorporacion")){
                        resultado = modelo.mostrarBusqueda(tabla,"fecha_incorporacion","'"+txtBusqueda.getText()+"%'");
                    }
                    else{
                        resultado = modelo.mostrarBusqueda(tabla,dcbmBusqueda.getSelectedItem().toString().toLowerCase(),"'"+txtBusqueda.getText()+"%'");
                    }
                }
                if(tabla.equals("Vehiculo")){
                    if(dcbmBusqueda.getSelectedItem().toString().equals("Propietario")){
                        resultado = modelo.mostrarBusquedaRelacion(tabla,"Cliente","cliente","dni","'"+txtBusqueda.getText()+"%'");
                    }
                    else if(dcbmBusqueda.getSelectedItem().toString().equals("Numero de chasis")){
                        resultado = modelo.mostrarBusqueda(tabla,"numero_chasis","'"+txtBusqueda.getText()+"%'");
                    }
                    else if(dcbmBusqueda.getSelectedItem().toString().equals("Fecha de Matriculacion")){
                        resultado = modelo.mostrarBusqueda(tabla,"fecha_matriculacion","'"+txtBusqueda.getText()+"%'");
                    }
                    else{
                        resultado = modelo.mostrarBusqueda(tabla,dcbmBusqueda.getSelectedItem().toString().toLowerCase(),"'"+txtBusqueda.getText()+"%'");
                    }
                }
                if(tabla.equals("Reparacion")){
                    if(dcbmBusqueda.getSelectedItem().toString().equals("Matricula Vehiculo")){
                        resultado = modelo.mostrarBusquedaRelacion(tabla,"Vehiculo","vehiculo","matricula","'"+txtBusqueda.getText()+"%'");
                    }
                    else if(dcbmBusqueda.getSelectedItem().toString().equals("Fecha de Emision")){
                        resultado = modelo.mostrarBusqueda(tabla,"fecha_emision","'"+txtBusqueda.getText()+"%'");
                    }
                    else if(dcbmBusqueda.getSelectedItem().toString().equals("Fecha de Entrega")){
                        resultado = modelo.mostrarBusqueda(tabla,"fecha_finalizacion","'"+txtBusqueda.getText()+"%'");
                    }

                    else{
                        resultado = modelo.mostrarBusqueda(tabla,dcbmBusqueda.getSelectedItem().toString().toLowerCase(),"'"+txtBusqueda.getText()+"%'");
                    }
                }
                if(tabla.equals("Cliente")){
                    if(chBusqueda.isSelected()){
                        if (dcbmBusqueda.getSelectedItem().toString().equals("Fecha de Nacimiento")) {
                            resultado = modelo.mostrarBusqueda2Criterios(tabla, "fecha_nacimiento","socio", "'" + txtBusqueda.getText() + "%'");
                        } else if (dcbmBusqueda.getSelectedItem().toString().equals("Telefono de Contacto")) {
                            resultado = modelo.mostrarBusqueda2Criterios(tabla, "telefono","socio", "'" + txtBusqueda.getText() + "%'");
                        } else if (dcbmBusqueda.getSelectedItem().toString().equals("Codigo Postal")) {
                            resultado = modelo.mostrarBusqueda2Criterios(tabla, "codigo_postal","socio", "'" + txtBusqueda.getText() + "%'");
                        } else if (dcbmBusqueda.getSelectedItem().toString().equals("Direccion")) {
                            resultado = modelo.mostrarBusqueda2Criterios(tabla, "domicilio","socio", "'" + txtBusqueda.getText() + "%'");
                        }else {
                            resultado = modelo.mostrarBusqueda2Criterios(tabla, dcbmBusqueda.getSelectedItem().toString().toLowerCase(),"socio", "'" + txtBusqueda.getText() + "%'");
                            }
                    }else {
                        if (dcbmBusqueda.getSelectedItem().toString().equals("Fecha de Nacimiento")) {
                            resultado = modelo.mostrarBusqueda(tabla, "fecha_nacimiento", "'" + txtBusqueda.getText() + "%'");
                        } else if (dcbmBusqueda.getSelectedItem().toString().equals("Telefono de Contacto")) {
                            resultado = modelo.mostrarBusqueda(tabla, "telefono", "'" + txtBusqueda.getText() + "%'");
                        } else if (dcbmBusqueda.getSelectedItem().toString().equals("Codigo Postal")) {
                            resultado = modelo.mostrarBusqueda(tabla, "codigo_postal", "'" + txtBusqueda.getText() + "%'");
                        } else if (dcbmBusqueda.getSelectedItem().toString().equals("Direccion")) {
                            resultado = modelo.mostrarBusqueda(tabla, "domicilio", "'" + txtBusqueda.getText() + "%'");
                        }else {
                                resultado = modelo.mostrarBusqueda(tabla, dcbmBusqueda.getSelectedItem().toString().toLowerCase(), "'" + txtBusqueda.getText() + "%'");
                            }
                        }
                }

                for(Object objeto: resultado){
                    dlmListaBusqueda.addElement(objeto);
                }


            }
            else{
                dlmListaBusqueda.removeAllElements();
            }

        }
    }
}
