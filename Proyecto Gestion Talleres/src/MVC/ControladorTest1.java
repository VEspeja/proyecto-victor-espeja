package MVC;

import base.Taller;
import org.apache.hadoop.hive.ql.udf.generic.GenericUDAFEvaluator;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.awt.event.ActionEvent;
import java.time.LocalDate;

import static org.junit.Assert.*;

public class ControladorTest1 extends Controlador {
    private static Taller taller;
    private Vista vista;
    private Modelo modelo;
    private VistaAcceso vistaAcceso;
    private Controlador controlador;
    private VistaGestionUsuarios vistaGestionUsuarios;
    public ControladorTest1() {
        this.vista = new Vista();
        this.modelo = new Modelo();
        this.vistaAcceso = new VistaAcceso();
        this.vistaGestionUsuarios = new VistaGestionUsuarios();
        this.controlador = new Controlador(vistaAcceso,vista,vistaGestionUsuarios,modelo);
    }
    @Before
    public void setUp() throws Exception {
        taller = new Taller("prueba","prueba",111222333, LocalDate.now(),"8","13",false);
    }

    @Test
    public void visualizarMecanico(){
        vista.txtNombreTaller.setText("" + taller.getNombre());
        vista.txtUbicacionTaller.setText("" + taller.getUbicacion());
        vista.txtTelefonoTaller.setText("" + taller.getTelefono());
        vista.dtFechaAperturaTaller.setDate(LocalDate.parse("" + taller.getFechaApertura()));
        vista.txtHorarioAperturaTaller.setText("" + taller.getHorarioApertura());
        vista.txtHorarioCierreTaller.setText("" + taller.getHorarioCierre());
        Assert.assertTrue("se ha visualizado correctamente",vista.txtNombreTaller.getText().equals("prueba"));
    }
    @Test
    public void loguearse(){
        vistaAcceso.txtUsuario.setText("admin");
        vistaAcceso.txtContrasenya.setText("admin");
        controlador.getVistaAcceso().btnAcceder.doClick();
        //Assert.assertTrue("Se ha logueado correctamente");
    }
}