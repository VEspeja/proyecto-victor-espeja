package MVC;

import base.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import principal.HibernateUtil;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

public class Modelo {
    private static SessionFactory sessionFactory;
    private static Session session;
    HibernateUtil hibernate;
    boolean containsAdmin;

    public Modelo(){
        hibernate = new HibernateUtil();
        containsAdmin = false;
    }

    public void conectar(){

        hibernate.buildSessionFactory();
        hibernate.openSession();
        session = hibernate.getCurrentSession();


        List<Usuario> lista = getUsuarios();
        for (Usuario usuario : lista){
            if(usuario.getRol() != 1 && containsAdmin == false){
                containsAdmin = false;
            }
            if(usuario.getRol() == 1){
                containsAdmin = true;
            }
        }
        Usuario admin = new Usuario("admin","admin",1);
        System.out.println(containsAdmin);
        if (containsAdmin == false){
            session.beginTransaction();
            session.save(admin);
            session.getTransaction().commit();
        }
    }

    public void desconectar(){
        hibernate.closeSessionFactory();
    }

    public void altaUsuario(String nameUser,String pass, String rol){
        Usuario usuario;
        if(rol.equals("Empleado")) {
           usuario = new Usuario(nameUser, pass, 2);
        }
        else{
            usuario = new Usuario(nameUser, pass, 3);
        }
        session.beginTransaction();
        session.save(usuario);
        session.getTransaction().commit();
    }
    public void altaUsuario(Usuario usuario){
        session.beginTransaction();
        session.save(usuario);
        session.getTransaction().commit();
    }
    public void altaUsuarioTest(Usuario usuario){
        if(session == null){
            hibernate.buildSessionFactory();
            hibernate.openSession();
            session = hibernate.getCurrentSession();
        }
        session.beginTransaction();
        session.save(usuario);
        session.getTransaction().commit();
    }
    public void modificarUsuario(Usuario usuario){
        session.beginTransaction();
        session.saveOrUpdate(usuario);
        session.getTransaction().commit();

    }
    public void eliminarUsuario(Usuario usuario){
        session.beginTransaction();
        session.delete(usuario);
        session.getTransaction().commit();
    }

    public void altaTaller(String nombre, String ubicacion, int telefono,LocalDate fechaApertura, String horarioApertura, String horarioCierre, boolean socio) {
        Taller taller = new Taller(nombre,ubicacion,telefono,fechaApertura,horarioApertura,horarioCierre,socio);
        session.beginTransaction();
        session.save(taller);
        session.getTransaction().commit();
    }
    public void altaTaller(Taller taller){
        session.beginTransaction();
        session.save(taller);
        session.getTransaction().commit();
    }
    public void modificarTaller(Taller taller){
        session.beginTransaction();
        session.saveOrUpdate(taller);
        session.getTransaction().commit();

    }
    public void modificarTallerTest(Taller taller){
        session.beginTransaction();
        session.saveOrUpdate(taller);
        session.getTransaction().commit();

    }
    public void eliminarTaller(Taller taller){
        session.beginTransaction();
        session.delete(taller);
        session.getTransaction().commit();
    }

    public void altaMecanico(String nombre, String apellidos, String dni, String numeroEmpleado, LocalDate fechaIncorporacion, int anyosExperiencia, String especialidad, Taller taller){
        Mecanico mecanico = new Mecanico(nombre,apellidos,dni,numeroEmpleado,fechaIncorporacion,anyosExperiencia,especialidad,taller);
        taller.getListaMecanicos().add(mecanico);
        session.beginTransaction();
        session.save(mecanico);
        session.getTransaction().commit();
    }
    public void altaMecanico(Mecanico mecanico){
        session.beginTransaction();
        session.save(mecanico);
        session.getTransaction().commit();
    }
    public void modificarMecanico(Mecanico mecanico){
        session.beginTransaction();
        session.saveOrUpdate(mecanico);
        session.getTransaction().commit();

    }
    public void eliminarMecanico(Mecanico mecanico){
        session.beginTransaction();
        session.delete(mecanico);
        session.getTransaction().commit();
    }

    public void altaReparacion(LocalDate fechaEmision, double coste, LocalDate fechaFinalizacion, String diagnostico, Vehiculo vehiculo){
        Reparacion reparacion = new Reparacion(fechaEmision,coste,fechaFinalizacion,diagnostico,vehiculo);
        vehiculo.getListaReparaciones().add(reparacion);
        session.beginTransaction();
        session.save(reparacion);
        session.getTransaction().commit();
    }
    public void altaReparacionTest(Reparacion reparacion){
        session = HibernateUtil.getCurrentSession();
        session.beginTransaction();
        session.save(reparacion);
        session.getTransaction().commit();
    }
    public void modificarReparacion(Reparacion reparacion){
        session.beginTransaction();
        session.saveOrUpdate(reparacion);
        session.getTransaction().commit();

    }
    public void eliminarReparacion(Reparacion reparacion){
        session.beginTransaction();
        session.delete(reparacion);
        session.getTransaction().commit();
    }
    public void eliminarReparacionTest(Reparacion reparacion){
        session = HibernateUtil.getCurrentSession();
        session.beginTransaction();
        session.delete(reparacion);
        session.getTransaction().commit();
    }

    public void altaVehiculo(int id, String modelo, String marca, String matricula, String combustible, LocalDate fechaMatriculacion, String numeroChasis, String imagen, Cliente cliente){
        System.out.println("llego");
        Vehiculo vehiculo = new Vehiculo(id,modelo,marca,matricula,combustible,fechaMatriculacion,numeroChasis,imagen,cliente);
        cliente.getListaVehiculos().add(vehiculo);
        session.beginTransaction();
        session.save(vehiculo);
        session.getTransaction().commit();
    }
    public void altaVehiculoTest(Vehiculo vehiculo){
        session = HibernateUtil.getCurrentSession();
        session.beginTransaction();
        session.save(vehiculo);
        session.getTransaction().commit();
    }
    public void modificarVehiculo(Vehiculo vehiculo){
        session.beginTransaction();
        session.saveOrUpdate(vehiculo);
        session.getTransaction().commit();

    }
    public void eliminarVehiculo(Vehiculo vehiculo){
        session.beginTransaction();
        session.delete(vehiculo);
        session.getTransaction().commit();
    }

    public void altaCliente(String nombre, String apellidos, String dni, int telefono, LocalDate fechaNacimiento, String domicilio, String codigoPostal, boolean socio){
        Cliente cliente = new Cliente(nombre,apellidos,dni,telefono,fechaNacimiento,domicilio,codigoPostal,socio);
        session.beginTransaction();
        session.save(cliente);
        session.getTransaction().commit();
    }
    public void altaCliente(Cliente cliente){
        session.beginTransaction();
        session.save(cliente);
        session.getTransaction().commit();
    }
    public void altaClienteTest(Cliente cliente){
        session = HibernateUtil.getCurrentSession();
        session.beginTransaction();
        session.save(cliente);
        session.getTransaction().commit();
    }
    public void modificarCliente(Cliente cliente){
        session.beginTransaction();
        session.saveOrUpdate(cliente);
        session.getTransaction().commit();

    }
    public void eliminarCliente(Cliente cliente){
        session.beginTransaction();
        session.delete(cliente);
        session.getTransaction().commit();
    }

    public void eliminarTallerTest(Taller taller) {
        session = HibernateUtil.getCurrentSession();
        session.beginTransaction();
        session.delete(taller);
        session.getTransaction().commit();
    }

    public List<Mecanico> obtenerMecanicosDisponiblesParaRepararReparacionAsignada(Reparacion reparacion) {
        List<Mecanico> lista = getMecanicosTallerReparacion(reparacion);
        lista.removeAll(reparacion.getListaMecanicos());
        return lista;
    }
    public List<Mecanico> obtenerMecanicosDisponiblesParaRepararReparacionLibre(Reparacion reparacion) {
        List<Mecanico> lista = getMecanicos();
        lista.removeAll(reparacion.getListaMecanicos());
        return lista;
    }

    public List<Taller> getTalleres(){
        Query query = session.createQuery("FROM Taller");
        List<Taller> lista = query.getResultList();
        return lista;
    }
    public List<Usuario> getUsuarios(){
        Query query = session.createQuery("FROM Usuario ");
        List<Usuario> lista = query.getResultList();
        return lista;
    }
    public List<Mecanico> getMecanicos(){
        Query query = session.createQuery("FROM Mecanico");
        List<Mecanico> lista = query.getResultList();
        return lista;
    }

    public List<Mecanico> getMecanicosTallerReparacion(Reparacion reparacion){
        int id_taller = 0;
        for (Mecanico mecanico: reparacion.getListaMecanicos()){
            id_taller = mecanico.getTaller().getId();
            break;
        }
        Query query = session.createQuery("select mec FROM Mecanico as mec, Taller as tal where tal.id = mec.taller and tal.id = "+id_taller);
        List<Mecanico> lista = query.getResultList();
        return lista;
    }

    public List<Cliente> getClientes(){
        Query query = session.createQuery("FROM Cliente");
        List<Cliente> lista = query.getResultList();
        return lista;
    }

    public List<Vehiculo> getVehiculos(){
        Query query = session.createQuery("FROM Vehiculo");
        List<Vehiculo> lista = query.getResultList();
        return lista;
    }
    public List<Reparacion> getReparaciones(){
        Query query = session.createQuery("FROM Reparacion");
        List<Reparacion> lista = query.getResultList();
        return lista;
    }

    public List mostrarBusqueda(String tabla, String campo, String valor) {
        Query query = session.createQuery("FROM "+tabla+" where "+campo+" like "+valor);
        List lista = query.getResultList();
        return lista;
    }
    public String mostrarBusquedaTaller(String valor) {
        Query query = session.createQuery("select cli.nombre FROM Taller as cli where cli.ubicacion like "+valor);
        String nombre = query.getQueryString();
        return nombre;
    }

    protected Taller buscarTallerUbicacion(String valor) {
       session = HibernateUtil.getCurrentSession();
       session.beginTransaction();

       Query query = session.createQuery("FROM Taller t where t.ubicacion like :value");
       query.setString("value",valor);
       List lista=query.getResultList();
       Taller taller = null;

       if(lista.size() != 0) {
           taller = (Taller) lista.get(query.getFirstResult());
       }
       session.getTransaction().commit();

       return taller;
   }

    protected Usuario buscarUsuarioNombre(String valor) {
        session = HibernateUtil.getCurrentSession();
        session.beginTransaction();

        Query query = session.createQuery("FROM Usuario t where t.nameUser like :value");
        query.setString("value",valor);
        List lista=query.getResultList();
        Usuario user = null;

        if(lista.size() != 0) {
            user = (Usuario) lista.get(query.getFirstResult());
        }
        session.getTransaction().commit();

        return user;
    }


    public List mostrarBusquedaRelacion(String tabla1, String tabla2,String valorTabla1, String campo, String valor) {
        Query query = session.createQuery("select a FROM "+tabla1+" as a,"+tabla2+" as b where b.id = a."+valorTabla1+" and b."+campo+" like "+valor);
        List lista = query.getResultList();
        return lista;
    }


    public List mostrarBusqueda2Criterios(String tabla, String campo, String campo2, String valor) {
        Query query = session.createQuery("FROM "+tabla+" where "+campo+" like "+valor +" and "+campo2+ " = true");
        List lista = query.getResultList();
        return lista;
    }

    public List<Integer> recogerAnyosReparaciones() {
        Query query = session.createQuery("select distinct year(rep.fechaEmision) FROM Reparacion as rep order by year(rep.fechaEmision)");
        List<Integer> lista = query.getResultList();
        return lista;
    }
}
