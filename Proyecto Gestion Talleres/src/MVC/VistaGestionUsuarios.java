package MVC;

import base.Usuario;

import javax.swing.*;
import java.util.Properties;

public class VistaGestionUsuarios {
    public JFrame frame;
    private JPanel panel1;
    public JTextField txtUsuario;
    public JTextField txtPass;
    public JList listaUsuarios;
    public JButton btnRegistrar;
    public JButton btnEliminar;
    public JButton btnModificar;
    public JComboBox cbRolUsuario;
    public JLabel etUsuarioGestion;
    public JLabel etPassGestion;
    public JLabel etRolGestion;

    public DefaultListModel<Usuario> dlmUsuarios;
    public DefaultComboBoxModel<String> dcbmRol;

    public VistaGestionUsuarios() {
        frame = new JFrame();
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.pack();
        frame.setVisible(false);
        frame.setLocationRelativeTo(null);
        ImageIcon logo = new ImageIcon("imagenes/iconos/logo_aplicacion.png");
        frame.setIconImage(logo.getImage());

        dlmUsuarios = new DefaultListModel<>();
        listaUsuarios.setModel(dlmUsuarios);
        dcbmRol = new DefaultComboBoxModel<>();
        cbRolUsuario.setModel(dcbmRol);

        dcbmRol.addElement("Empleado");
        dcbmRol.addElement("Consultor");
        btnRegistrar.setIcon(new ImageIcon("imagenes/iconos/add-user.png"));
        btnModificar.setIcon(new ImageIcon("imagenes/iconos/edit-user.png"));
        btnEliminar.setIcon(new ImageIcon("imagenes/iconos/delete-user.png"));
    }

}
