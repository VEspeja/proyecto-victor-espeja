package MVC;

import javax.swing.*;

public class SplashScreen extends JFrame implements Runnable {
    private JLabel imagen;
    Modelo modelo;
    Thread hilo;
    public SplashScreen(Modelo modelo){
        this.modelo = modelo;
        imagen = new JLabel(new ImageIcon("imagenes/logo_splash.png"));
    }
    @Override
    public void run() {

        this.setContentPane(imagen);
        this.setUndecorated(true);
        this.setIconImage(new ImageIcon("imagenes/iconos/logo_aplicacion.png").getImage());
        this.pack();
        this.setLocationRelativeTo(null);
        this.setVisible(true);

        modelo.conectar();
        try {
            hilo.sleep(6000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        this.dispose();

    }
}
