package MVC;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Objects;

public class VistaAcceso {
    private JPanel panel1;
    public JTextField txtUsuario;
    public JPasswordField txtContrasenya;
    public JButton btnAcceder;
    public JComboBox cbIdioma;
    public JLabel etUsuarioLogin;
    public JLabel etPassLogin;
    public JLabel etSeleccionIdioma;
    public JFrame frame;
    public DefaultComboBoxModel dcbmIdioma;
    public ImageIcon spanish;
    public ImageIcon english;

    public VistaAcceso() {
        frame = new JFrame("Gestor de Talleres v.1.0");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(false);
        frame.pack();
        frame.setVisible(false);
        ImageIcon logo = new ImageIcon("imagenes/iconos/logo_aplicacion.png");
        frame.setIconImage(logo.getImage());
        frame.setLocationRelativeTo(null);

        /*Object[] items =
                {
                        new ImageIcon("about16.gif"),
                        new ImageIcon("add16.gif"),
                        new ImageIcon("copy16.gif")
                };
        cbIdioma = new JComboBox( items );
         */

        btnAcceder.setActionCommand("Acceder");
        dcbmIdioma = new DefaultComboBoxModel();
        cbIdioma.setModel(dcbmIdioma);

        //dcbmIdioma.addElement("Español");
        //dcbmIdioma.addElement("English");

        spanish = new ImageIcon("imagenes/españa.png");
        english = new ImageIcon("imagenes/english.png");

        dcbmIdioma.addElement(spanish);
        dcbmIdioma.addElement(english);
    }


}
