create database gestion_talleres;

use gestion_talleres;

create table usuario(
id int primary key auto_increment,
name_user varchar(50) not null unique,
pass varchar(50) not null,
rol int
);



create table taller(
id int primary key auto_increment,
nombre varchar(50) not null,
ubicacion varchar(200) not null,
telefono int(9) not null,
fecha_apertura date not null,
horario_apertura varchar(5) not null,
horario_cierre varchar(5) not null,
tienda_online boolean not null
);


create table mecanico(
id int primary key auto_increment,
nombre varchar(50) not null not null,
apellidos varchar(50) not null not null,
dni varchar(9) unique,
numero_empleado varchar(15) not null,
fecha_incorporacion date not null,
anyos_experiencia int not null,
especialidad varchar(50) not null,
id_taller int,
foreign key (id_taller) references taller (id) on delete set null
);

create table cliente(
id int primary key auto_increment,
nombre varchar(50) not null,
apellidos varchar(100) not null not null,
dni varchar(9) unique,
telefono int(9) not null unique,
fecha_nacimiento date not null,
domicilio varchar(100) not null,
codigo_postal varchar(6) not null,
socio boolean not null
);

create table vehiculo(
id int,
modelo varchar(50) not null,
marca varchar(50) not null,
matricula varchar(7) not null,
combustible varchar(30)not null,
fecha_matriculacion date not null,
numero_chasis varchar(30) not null,
imagen varchar(150) not null,
id_propietario int,
primary key (matricula),
foreign key (id_propietario) references cliente (id) on delete set null
);

create table reparacion(
id int primary key auto_increment,
fecha_emision date not null,
coste double not null,
fecha_finalizacion date not null,
diagnostico varchar(500) not null,
matricula_vehiculo varchar(7),
foreign key (matricula_vehiculo) references vehiculo (matricula) on delete set null
);

create table reparacion_mecanico(
id_reparacion int,
id_mecanico int,
primary key(id_reparacion, id_mecanico),
foreign key (id_reparacion) references reparacion (id),
foreign key (id_mecanico) references mecanico (id)
);



